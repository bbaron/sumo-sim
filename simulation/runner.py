#!/usr/bin/env python
import os, sys
import optparse
import subprocess
import random

# we need to import python modules from the $SUMO_HOME/tools directory
try:
    sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..', '..', '..', "tools")) # tutorial in tests
    sys.path.append(os.path.join(os.environ.get("SUMO_HOME", os.path.join(os.path.dirname(__file__), "..", "..", "..")), "tools")) # tutorial in docs
    from sumolib import checkBinary
except ImportError:
    sys.exit("please declare environment variable 'SUMO_HOME' as the root directory of your sumo installation (it should contain folders 'bin', 'tools' and 'docs')")

import traci
# the port used for communicating with your sumo instance
PORT = 8873

def getOffloadingSpot():
    pass

def run():
    """execute the TraCI control loop"""
    traci.init(PORT)
    step = 0
    while step < 10000:
        # traci.simulation.getMinExpectedNumber()
        traci.simulationStep()
        
        # Get the vehicles ids that started parking
        startPark = traci.simulation.getParkingStartingVehiclesIDList()
        # Get the vehicles ids that ended parking
        endPark = traci.simulation.getParkingEndingVehiclesIDList()
        for vehId in startPark:
            vehType = traci.vehicle.getTypeID(vehId)
            vehPos  = traci.vehicle.getPosition(vehId)
            vehStops = traci.vehicle.getStops(vehId)
            vehEdge = traci.vehicle.getRoadID(vehId)
            stopID  = traci.vehicle.getBusStopID(vehId)
            print vehId+" just started parking at "+str(step)+ " type is "+vehType+" vehicle stops are ", vehStops
        # for vehId in endPark:
        #     vehType = traci.vehicle.getTypeID(vehId)
        #     vehPos  = traci.vehicle.getPosition(vehId)
        #     vehLane = traci.vehicle.getLaneID(vehId)
        #     print vehId+" just exited parking at "+str(step)+" type is "+vehType+" vehicle position is "+str(vehPos)+" on lane "+vehLane

        step += 1

    traci.close()
    sys.stdout.flush()


def get_options():
    optParser = optparse.OptionParser()
    optParser.add_option("--nogui", action="store_true", default=False, help="run the commandline version of sumo")
    options, args = optParser.parse_args()
    return options


# this is the main entry point of this script
if __name__ == "__main__":
    options = get_options()

    # this script has been called from the command line. It will start sumo as a
    # server, then connect and run
    if options.nogui:
        sumoBinary = checkBinary('sumo')
    else:
        sumoBinary = checkBinary('sumo-gui')

    # this is the normal way of using traci. sumo is started as a
    # subprocess and then the python script connects and runs
    sumoProcess = subprocess.Popen([sumoBinary, "-c", "sim.sumo.cfg", "--remote-port", str(PORT)],  stdout=sys.stdout, stderr=sys.stderr)
    run()
    sumoProcess.wait()
