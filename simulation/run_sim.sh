#!/bin/bash
array=(0.1 0.25 0.5)
array1=(0.1 0.25 0.5 0.75 1 1.25 1.5 1.75 2 2.5 3 3.5 4 4.5 5)
array2=(RR BUF TRF TRF1)

for sch in ${array2[@]}
do
  for split in ${array[@]}
  do
    for alpha in ${array1[@]}
    do
      python runner2.py results2/throughput_inf_$sch.txt $alpha $split $sch infinite
    done
  done
done