d$grp <- paste(d$V1, d$V4)
dat.m = melt(d, id.var=c("V5", "grp"), measure.vars = c("V7"))
ggplot(dat.m, aes(x=V5, y=value, group=grp, colour=as.factor(grp))) + 
  geom_line() + 
  geom_point() + 
  xlab("Alpha") +   # Remove x-axis label
  ylab("Maximum throughput") + 
  guides(col = guide_legend(nrow = 3, title = "Target and split ratio",))