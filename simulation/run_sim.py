#!/usr/bin/env python
from __future__ import division
import os, sys
import subprocess
import shlex
import csv
from math import floor

class AutoVivification(dict):
    """Implementation of perl's autovivification feature."""
    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value

schedulers = ['RR', 'BUF', 'TRF', 'TRF1']
for sch in schedulers:
	in_filename  = "results2/throughput_inf_"+sch+".txt"
	out_filename = "results2/throughput_poi_"+sch+".txt"

	res = AutoVivification()
	with open(in_filename, 'r') as f:
		reader = csv.reader(f, delimiter=';')
		for row in reader:
			target = row[0]
			gamma_S, gamma_I = float(row[1]), float(row[2])
			split_ratio, alpha = float(row[3]), float(row[4])
			max_throughput = float(row[6])
			res[a][b][target] = floor(rate * 100) / 100.0

	for a in res.keys():
		for b in res[a].keys():
			r1 = res[a][b]['T1']
			r2 = res[a][b]['T2']
			cmd = sys.executable + " runner2.py "+out_filename+" "+str(b)+" "+str(a)+" "+sch+" "+str(r1)+" "+str(r2)
			print cmd
			subprocess.call(shlex.split(cmd))
