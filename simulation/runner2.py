#!/usr/bin/env python
from __future__ import division
import os, sys
import optparse
import subprocess
import random
import itertools
import math
import time
import numpy as np
import networkx as nx
import cplex
from cplex.exceptions import CplexSolverError

# we need to import python modules from the $SUMO_HOME/tools directory
try:
    sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..', '..', '..', "tools")) # tutorial in tests
    sys.path.append(os.path.join(os.environ.get("SUMO_HOME", os.path.join(os.path.dirname(__file__), "..", "..", "..")), "tools")) # tutorial in docs
    from sumolib import checkBinary
except ImportError:
    sys.exit("please declare environment variable 'SUMO_HOME' as the root directory of your sumo installation (it should contain folders 'bin', 'tools' and 'docs')")

import traci
# the port used for communicating with your sumo instance
PORT = 8873

class Data(object):
    """ This class represents a chunk of data that a vehicle can carry """
    def __init__(self, target, startTime=0):
        self.target = target
        self.startTime = startTime
        self.loadTime = startTime
        self.arrivedAtIntermediate = -1
        self.leftIntermediate = -1
        self.endTime = -1

    def getTarget(self):
        return self.target

    def __str__(self):
        return "# Data " + self.target + " #"

class Vehicle(object):
    """ This class represents a vehicle that is equipped with data storage device (of type1) """
    def __init__(self, vehID, vehRoute):
        self.vehID = vehID
        self.vehRoute = vehRoute
        self.data = None
        self.sharesGPSInfo = sharesGPSInfo()

    def hasData(self):
        return self.data

    def loadData(self, data):
        self.data = data

    def unloadData(self):
        data = self.data
        self.data = None
        return data

    def getRoute(self):
        return self.vehRoute

    def getAllStops(self):
        return traci.vehicle.getStops(self.vehID)

    def getData(self):
        return self.data

    def getDestination(self):
        return self.vehRoute[-1]

    def getSource(self):
        return self.vehRoute[1]

    def isSharingGPSInfo(self):
        return self.sharesGPSInfo


myVehicles = dict() # Global dictionary that stores all of the vehicles equipped with data storage devices
SOURCE_EDGE = 'e1'
INTERMEDIATE_EDGE_OUT = 'e2'
INTERMEDIATE_EDGE_IN = ['e3', 'e5']
TARGET_EDGE_1 = 'e4'
TARGET_EDGE_2 = 'e6'
TARGET_1 = 'T1'
TARGET_2 = 'T2'
PENETRATION_RATIO = 0.1
SIM_TIME = 5000 # Simulation time (in sceonds)
STOP_PROB = 0
SCHEDULER_TYPE = 'RR' # enum('RR', 'BUF', 'TRF', 'TRF_BUF')
ARRIVAL_TYPE = 'infinite' # arrivalType in enum('fixed', 'Poisson', 'infinite')

split_ratio = 0.5
gamma_S, gamma_I = 3600, 3600 # vehicles per hour
alpha = 1
beta = 1
rate_1,rate_2 = 1,1

output_file = 'results/res.txt'


if 1 < len(sys.argv):
    output_file = sys.argv[1]
    alpha, split_ratio = float(sys.argv[2]), float(sys.argv[3])
    SCHEDULER_TYPE = sys.argv[4]
    ARRIVAL_TYPE = sys.argv[5]

    if ARRIVAL_TYPE == "Poisson":
        rate_1 = float(sys.argv[6])
        beta = float(sys.argv[7])
    
    gamma_S = gamma_I * alpha
    rate_2 = beta * rate_1

DATA_RATE = {
    TARGET_EDGE_1: rate_1, 
    TARGET_EDGE_2: rate_2
}

DATA_SIZE = {
    TARGET_EDGE_1: 100,
    TARGET_EDGE_2: 100
}

if ARRIVAL_TYPE == 'Poisson':
    dataAtSource = { TARGET_EDGE_1: [], TARGET_EDGE_2: []}
else:
    dataAtSource = dict([(target, [Data(target)]*size) for (target, size) in DATA_SIZE.items()])
dataAtIntermediate = { TARGET_EDGE_1: [], TARGET_EDGE_2: [] }
dataAtTarget = { TARGET_EDGE_1: [], TARGET_EDGE_2: [] }

ROAD_NETWORK = nx.Graph()
ROAD_NETWORK.add_edge('S', 'A1', name='e1')
ROAD_NETWORK.add_edge('A1', 'I', name='e2')
ROAD_NETWORK.add_edge('I', 'A2', name='e3')
ROAD_NETWORK.add_edge('A2', 'T1', name='e4')
ROAD_NETWORK.add_edge('I', 'A3', name='e5')
ROAD_NETWORK.add_edge('A3', 'T2', name='e6')

# the trip matrix gives the proportion of traffic between the nodes
TRIP_MATRIX = {
    'S':  {'A1': 0, 'I': 1, 'A2': 0, 'A3': 0, 'T1': 0, 'T2': 0},
    'A1': {'I': 0.33, 'A2': 0, 'A3': 0, 'T1': 0.33, 'T2': 0.33},
    'I':  {'A2': 0, 'A3': 0, 'T1': split_ratio, 'T2': (1.0-split_ratio)}
}

# the vehicle traffic repartition gives the probability a vehicle enters the simulation at each step
TRAFFIC_REP = {'S': gamma_S, 'A1': 0, 'I': gamma_I}


def translate_target(target):
    if target == TARGET_EDGE_1: return TARGET_1
    if target == TARGET_EDGE_2: return TARGET_2

def arrivalGenerator(rate):
    t = 0
    while True:
        t += random.expovariate(rate)
        yield t

def generateArrivals():
    """ Generate the list of Poisson arrivals for the two data streams """
    t = [(i, 0) for i in DATA_RATE.keys()]
    arrivals = []
    for i in DATA_RATE.keys():
        t = 0
        if DATA_RATE[i] > 0:
            generator = arrivalGenerator(DATA_RATE[i])
            arrivals += [(i, arrival) \
                         for arrival in itertools.takewhile(lambda t: t < SIM_TIME, generator)]

    sorted_arrivals = sorted(arrivals, key=lambda x: x[1])

    return sorted_arrivals

def myRound(x):
    if x >= 1: return round(x)
    return x

def is_target(node):
    return node == 'T1' or node == 'T2'

def get_edges_from_nodes(path):
    return [ROAD_NETWORK[i][j]['name'] for (i,j) in zip(path[0:], path[1:])]

def get_edges_to_dest(src, dest):
    return get_edges_from_nodes(nx.dijkstra_path(ROAD_NETWORK, src, dest))

def generate_routefile():
    """ This function generates the route file from the attributes given in the command line """
    stop_str = '<stop lane="e2_0" duration="50" parking="true"/>'

    with open("sim1.rou.xml", "w") as routes:
        print >> routes, """<routes>
    <!-- Types of vehicles -->
    <vTypeDistribution id="typedist1">"""
        print >> routes, '        <vType id="type1" accel="0.8" vClass="passenger"  length="5" maxSpeed="70" probability="%.2f" color="0,0,1" />' % (1.0-PENETRATION_RATIO)
        print >> routes, '        <vType id="type2" accel="0.8" vClass="passenger"  length="5" maxSpeed="70" probability="%.2f" color="0,1,1" />' % (PENETRATION_RATIO)
        print >> routes, "    </vTypeDistribution>"

        route_sum = 0
        route_dist_list = []
        for src in TRIP_MATRIX.keys():
            print >> routes, '    <!-- Routes from node %s -->' % (src)
            print >> routes, '    <routeDistribution id="routedist-%s">' % (src)
            sum_proba = sum([TRIP_MATRIX[src][dest] for dest in TRIP_MATRIX[src].keys()])
            for dest in TRIP_MATRIX[src].keys():
                ledges = get_edges_to_dest(src, dest)
                proba = TRIP_MATRIX[src][dest]
                if proba == 0.0: continue
                if STOP_PROB > 0 and is_target(dest) and 'e2' in ledges:
                    print >> routes, '         <route id="route%d" edges="%s" probability="%.2f"/>' % (route_sum, " ".join(ledges), proba*(1-STOP_PROB)/sum_proba)
                    route_sum += 1
                    print >> routes, '         <route id="route%d" edges="%s" probability="%.2f">' % (route_sum, " ".join(ledges), proba*STOP_PROB/sum_proba)
                    print >> routes, '             '+stop_str
                    print >> routes, '         </route>'
                else:
                    print >> routes, '         <route id="route%d" edges="%s" probability="%.2f"/>' % (route_sum, " ".join(ledges), proba/sum_proba)
                route_sum += 1
            print >> routes, '     </routeDistribution>'

        print >> routes, '    <!-- Flows -->'
        flow_sum = 0
        for route_dist in TRAFFIC_REP.keys():
            rate = TRAFFIC_REP[route_dist]
            if rate > 0:
                print >> routes, '    <flow id="flow%d" departPos="0" departLane="random" departSpeed="max" type="typedist1" route="routedist-%s" begin="0.00" vehsPerHour="%.2f"/>' % (flow_sum, route_dist, rate)
            flow_sum += 1
        print >> routes, '</routes>'

def weightedScheduler():
    sumSizes = sum([size for size in DATA_SIZE.values()])
    weights = dict([(target, int(100*size / sumSizes)) for (target, size) in DATA_SIZE.items()])
    targetList = sum([[target]*w for (target, w) in weights.items()], [])

    while True:
        yield random.choice(targetList)

def roundRobinScheduler(sequence):
    i = 0
    while True:
        j = 0
        while j < len(sequence) and len(dataAtSource[sequence[i]]) == 0:
            i = (i + 1) % len(sequence)
            j += 1
        if j == len(sequence):
            yield None
        else:
            yield sequence[i]
        i = (i + 1) % len(sequence)


def bufferWeightedScheduler():
    while True:
        data_1 = len(dataAtSource[TARGET_EDGE_1])
        data_2 = len(dataAtSource[TARGET_EDGE_2])
        if data_1 == 0 and data_2 == 0:
            yield None
        else:
            weight_target_1 = int( 100 * data_1 / ( data_1 + data_2 ) )
            weight_target_2 = int( 100 * data_2 / ( data_1 + data_2 ) )

            targetList = [TARGET_EDGE_1] * weight_target_1 + [TARGET_EDGE_2] * weight_target_2

            yield random.choice(targetList)

def detBufferWeightedScheduler():
    while True:
        data_1 = len(dataAtSource[TARGET_EDGE_1])
        data_2 = len(dataAtSource[TARGET_EDGE_2])
        if data_1 == 0 and data_2 == 0:
            yield None
        elif data_1 < data_2:
            yield TARGET_EDGE_2
        elif data_2 < data_1:
            yield TARGET_EDGE_1
        else:
            yield random.choice([TARGET_EDGE_1,TARGET_EDGE_2])


def trafficWeightedScheduler():
    while True:
        traffic_rate_I_T1 = TRIP_MATRIX['I']['T1'] * TRAFFIC_REP['I']
        traffic_rate_I_T2 = TRIP_MATRIX['I']['T2'] * TRAFFIC_REP['I']

        traffic_weight_1 = int( 100 * traffic_rate_I_T1 / ( traffic_rate_I_T1 + traffic_rate_I_T2 ) )
        traffic_weight_2 = int( 100 * traffic_rate_I_T2 / ( traffic_rate_I_T1 + traffic_rate_I_T2 ) )

        targetList = [TARGET_EDGE_1] * traffic_weight_1 + [TARGET_EDGE_2] * traffic_weight_2

        yield random.choice(targetList)

def trafficBufferWeightedScheduler():
    while True:
        data_int_1 = len(dataAtIntermediate[TARGET_EDGE_1])
        data_int_2 = len(dataAtIntermediate[TARGET_EDGE_2])

        traffic_rate_I_T1 = TRIP_MATRIX['I']['T1'] * TRAFFIC_REP['I']
        traffic_rate_I_T2 = TRIP_MATRIX['I']['T2'] * TRAFFIC_REP['I']

        if traffic_rate_I_T1 == 0 and traffic_rate_I_T2 != 0:
            yield TARGET_EDGE_2
        elif traffic_rate_I_T1 != 0 and traffic_rate_I_T2 == 0:
            yield TARGET_EDGE_1
        elif traffic_rate_I_T1 == 0 and traffic_rate_I_T2 == 0:
            yield None
        elif (data_int_1 / traffic_rate_I_T1) < (data_int_2 / traffic_rate_I_T2):
            yield TARGET_EDGE_1
        elif (data_int_1 / traffic_rate_I_T1) > (data_int_2 / traffic_rate_I_T2):
            yield TARGET_EDGE_2
        else:
            yield random.choice([TARGET_EDGE_1,TARGET_EDGE_2])

def multiCommodityFlow():
    traffic_rate_S_I  = TRIP_MATRIX['S']['I']  * TRAFFIC_REP['S']
    traffic_rate_S_T1 = TRIP_MATRIX['S']['T1'] * TRAFFIC_REP['S']
    traffic_rate_S_T2 = TRIP_MATRIX['S']['T2'] * TRAFFIC_REP['S']
    traffic_rate_I_T1 = TRIP_MATRIX['I']['T1'] * TRAFFIC_REP['I']
    traffic_rate_I_T2 = TRIP_MATRIX['I']['T2'] * TRAFFIC_REP['I']

    w_1 = DATA_SIZE_1 / ( DATA_SIZE_1 + DATA_SIZE_2 )
    w_2 = DATA_SIZE_2 / ( DATA_SIZE_1 + DATA_SIZE_2 )

    model = cplex.Cplex()
    model.set_results_stream(None)
    model.set_log_stream(None)
    model.set_error_stream(None)
    model.set_warning_stream(None)

    # Add the variables in the model
    obj   = [1.0, 1.0, 1.0, 1.0]                                             # Linear objective coefficients of the variables
    names = ["d1p1", "d1p2", "d2p1", "d2p2"]                                 # List of strings (variable names)
    lb    = [0.0, 0.0, 0.0, 0.0]                                             # List of lower bounds (for the variables)
    ub    = [cplex.infinity, cplex.infinity, cplex.infinity, cplex.infinity] # List of upper bounds (fro the variables)


    model.variables.add(obj = obj,          # Objectve factor
                        lb = lb,            # Lower bounds
                        ub = ub,            # Upper bounds
                        names = names)      # Variables name

    # Capacity constraint
    capacity_constraints = [ 
        cplex.SparsePair(ind = ["d1p1", "d2p1"], val = [1.0, 1.0]),
        cplex.SparsePair(ind = ["d1p2"], val = [1.0]),
        cplex.SparsePair(ind = ["d2p2"], val = [1.0]),
        cplex.SparsePair(ind = ["d1p1"], val = [1.0]),
        cplex.SparsePair(ind = ["d2p1"], val = [1.0]) ]
    capacity_senses = ['L', 'L', 'L', 'L', 'L']
    capacity_rhs = [traffic_rate_S_I, traffic_rate_S_T1, traffic_rate_S_T2, traffic_rate_I_T1, traffic_rate_I_T2]
    model.linear_constraints.add(lin_expr = capacity_constraints,
                                 senses   = capacity_senses,
                                 rhs      = capacity_rhs)

    # Demand constraints
    demand_constraints = [
        cplex.SparsePair(ind = ["d1p1", "d1p2"], val = [1.0, 1.0]),
        cplex.SparsePair(ind = ["d2p1", "d2p2"], val = [1.0, 1.0])]
    demand_senses = ['L', 'L']
    demand_rhs = [w_1, w_2]
    model.linear_constraints.add(lin_expr = demand_constraints,
                                 senses   = demand_senses,
                                 rhs      = demand_rhs)

    # Our objective is to Maximize the flows.
    model.objective.set_sense(model.objective.sense.maximize)

    try:
        model.solve()
    except CplexSolverError, e:
        print "Exception raised during solve: " + e
    else:
        solution = model.solution

    return solution


def checkVehType(vehID):
    # Continue if the vehicle is not of type1 (the vehicles that are equipped with storage devices)
    if traci.vehicle.getTypeID(vehID) == "type1":
        return True 
    else: 
        return False

def sharesGPSInfo():
    return True # bool(random.getrandbits(1))

def dataInTransit():
    """ Return the amount of data in transit (ie. carried by vehicles) """
    return len([veh for veh in myVehicles.values() if veh.getData()])

def atSource(vehID, scheduler, step):
    global myVehicles, dataAtSource, dataAtIntermediate, dataAtTarget

    veh = myVehicles[vehID]
    if veh.isSharingGPSInfo():
        # We have access to the vehicle's GPS information
        # Get the vehicle destination
        vehDest = veh.getDestination()
        if vehDest == INTERMEDIATE_EDGE_OUT:
            if ARRIVAL_TYPE == 'infinite' and len(dataAtSource[TARGET_EDGE_1]) == 0:
                # Add more data to the source storage
                dataAtSource[TARGET_EDGE_1] += [Data(TARGET_EDGE_1)]*DATA_SIZE[TARGET_EDGE_1]
            if ARRIVAL_TYPE == 'infinite' and len(dataAtSource[TARGET_EDGE_2]) == 0:
                # Add more data to the source storage
                dataAtSource[TARGET_EDGE_2] += [Data(TARGET_EDGE_2)]*DATA_SIZE[TARGET_EDGE_2]

            # Load data on the vehicle depending on the scheduler
            data_target = scheduler.next()
            if not data_target: # Do nothing
                return

            if len(dataAtSource[data_target]) > 0:
                data = dataAtSource[data_target].pop()
                data.loadTime = step
                veh.loadData(data)
        elif vehDest == TARGET_EDGE_1 or vehDest == TARGET_EDGE_2:
            # Load data on the vehicle
            if len(dataAtSource[vehDest]) > 0:
                data = dataAtSource[vehDest].pop()
                data.loadTime = step
                veh.loadData(data)

        # # Get the stops of the vehicle
  #       for stop in veh.getAllStops():
  #           if stop == INTERMEDIATE_EDGE_OUT:
  #               # Load data on the vehicle depending on the weighted scheduler (weights result from the flow allocation)
  #               veh.loadData(weightedScheduler())
    else: 
        # We do not have access to the vehicle GPS information
        # Do probablisitc routing
        pass


def atIntermediate(vehID, what, step):
    """ Forwarding process at the intermediate offloading spot
        "what" is what the vehicle does at the offloading spot: enum('pass', 'in', 'out') """

    global myVehicles, dataAtSource, dataAtIntermediate, dataAtTarget

    veh = myVehicles[vehID]
    if what == 'pass':
        # Vehicle passing (stoping) at the offloading spot before continuing its journey
        pass # Do nothing for now
    elif what == 'out':
        # Incoming vehicle that finishes its trip at the offloading spot => data drop-off
        data = veh.unloadData()
        if data:
            data.arrivedAtIntermediate = step
            dataAtIntermediate[data.getTarget()].append(data)
    elif what == 'in':
        # Outgoing vehicle starting its trip from the offloading spot => data pickup
        if veh.isSharingGPSInfo():
            # Get the vehicle's destination
            vehDest = veh.getDestination()
            if vehDest in [TARGET_EDGE_1, TARGET_EDGE_2]:
                if dataAtIntermediate[vehDest]:
                    data = dataAtIntermediate[vehDest].pop()
                    data.leftIntermediate = step
                    veh.loadData(data)
        else:
            pass


def atDestination(vehID, step):
    """ Function that models the forwaring process at the destination nodes """

    global myVehicles, dataAtSource, dataAtIntermediate, dataAtTarget

    veh = myVehicles[vehID]
    data = veh.unloadData()
    if not data:
        return
    target = data.getTarget()

    if target in [TARGET_EDGE_1,TARGET_EDGE_2]:
        data.endTime = step
        dataAtTarget[target].append(data)
    else:
        print "Forwarding error"
        return


def atLeakage(vehID):
     pass

def get_scheduler():
    if SCHEDULER_TYPE == 'RR':
        return roundRobinScheduler([TARGET_EDGE_1,TARGET_EDGE_2])
    elif SCHEDULER_TYPE == 'BUF':
        return bufferWeightedScheduler()
    elif SCHEDULER_TYPE == 'TRF':
        return trafficWeightedScheduler()
    elif SCHEDULER_TYPE == 'TRF1':
        return trafficBufferWeightedScheduler()

def run():
    """ execute the TraCI control loop
        -  """

    global myVehicles, dataAtSource, dataAtIntermediate, dataAtTarget
    
    traci.init(PORT)

    f = open("res1.txt", 'w')
    if ARRIVAL_TYPE == 'Poisson':
        arrivals = generateArrivals()

    scheduler = get_scheduler()
    print "Scheduler is " + SCHEDULER_TYPE + " and arrival type is " + ARRIVAL_TYPE

    # Define the scheduler, it nust be a generator
    # scheduler = weightedScheduler()
    # scheduler = roundRobinScheduler([TARGET_EDGE_1,TARGET_EDGE_2])
    # scheduler = bufferWeightedScheduler()
    # scheduler = detBufferWeightedScheduler()
    # scheduler = trafficBufferWeightedScheduler()
    # scheduler = trafficWeightedScheduler()

    step = 0
    while step < SIM_TIME:
        # traci.simulation.getMinExpectedNumber()
        traci.simulationStep()

        if ARRIVAL_TYPE == 'Poisson' :
            # Generate the data in the corresponding buffer according to a Poisson process
            while (len(arrivals) > 0) and (math.floor(arrivals[0][1]) == step):
                d = arrivals.pop(0)
                dataAtSource[d[0]].append(Data(d[0], startTime = step))

        # Get the vehicles ids that entered the simulation ("departed")
        enteredSim = traci.simulation.getDepartedIDList()
        for vehID in enteredSim:
            if not checkVehType(vehID):
                continue

            # Add the vehicle to the myVehicles dictionary
            vehRoute = traci.vehicle.getRoute(vehID)
            if not vehID in myVehicles:
                myVehicles[vehID] = Vehicle(vehID, vehRoute)
            
            edgeID = traci.vehicle.getRoadID(vehID)
            if edgeID == SOURCE_EDGE:
                atSource(vehID, scheduler, step)
            elif edgeID in INTERMEDIATE_EDGE_IN:
                atIntermediate(vehID, 'in', step)


        # Get the vehicles ids that exited the simulation ("arrived")
        exitedSim = traci.simulation.getArrivedIDList()
        for vehID in exitedSim:
            if vehID not in myVehicles:
                continue
            
            veh = myVehicles[vehID]
            if not veh.hasData(): 
                continue
            edgeID = veh.getDestination()
            if edgeID == INTERMEDIATE_EDGE_OUT:
                atIntermediate(vehID, 'out', step)
            elif edgeID in [TARGET_EDGE_1,TARGET_EDGE_2]:
                atDestination(vehID, step)
            else:
                atLeakage(vehID)

            # Delete the vehicle from the myVehicles dictionary
            del myVehicles[vehID]

        
        # Get the vehicles ids that started parking
        startPark = traci.simulation.getParkingStartingVehiclesIDList()
        # Get the vehicles ids that ended parking
        endPark = traci.simulation.getParkingEndingVehiclesIDList()
        for vehID in startPark:
            if not checkVehType(vehID):
                continue

            atIntermediate(vehID, 'pass')
            
        # for vehID in endPark:
        #     vehType = traci.vehicle.getTypeID(vehID)
        #     vehPos  = traci.vehicle.getPosition(vehID)
        #     vehLane = traci.vehicle.getLaneID(vehID)
        #     print vehID+" just exited parking at "+str(step)+" type is "+vehType+" vehicle position is "+str(vehPos)+" on lane "+vehLane

        step += 1

        f.write("%d,%d,%d,%d,%d,%d,%d,%d,%d,%d\n" % (
            step, 
            dataInTransit(), 
            len(dataAtSource[TARGET_EDGE_1]), 
            len(dataAtSource[TARGET_EDGE_2]), 
            len(dataAtIntermediate[TARGET_EDGE_1]), 
            len(dataAtIntermediate[TARGET_EDGE_2]), 
            len(dataAtTarget[TARGET_EDGE_1]), 
            len(dataAtTarget[TARGET_EDGE_2]), 
            len(enteredSim), 
            len(exitedSim)))

        # print step, dataInTransit(), len(dataAtSource[TARGET_EDGE_1]), len(dataAtSource[TARGET_EDGE_2]), len(dataAtIntermediate[TARGET_EDGE_1]), len(dataAtIntermediate[TARGET_EDGE_2]), len(dataAtTarget[TARGET_EDGE_1]), len(dataAtTarget[TARGET_EDGE_2]), len(enteredSim), len(exitedSim)
        if ARRIVAL_TYPE != 'infinite' and len(dataAtTarget[TARGET_EDGE_1]) == DATA_SIZE[TARGET_EDGE_1] and len(dataAtTarget[TARGET_EDGE_2]) == DATA_SIZE[TARGET_EDGE_2]:
            break

    # Compute statistics on the data arrived at destination
    with open(output_file, 'a') as f1:
        for target, data in dataAtTarget.items():
            to_target = translate_target(target)
            transitTime, transitTimeToIntermediate, transitTimeToTarget, timeAtSource, timeAtIntermediate = 0,0,0,0,0
            dataLength = len(data)
            i = 0
            # f1.write("start time;target;time at S;time at I;total time\n")
            for d in data:
                transitTime += (d.endTime - d.startTime)
                timeAtSource += (d.loadTime - d.startTime)
                transitTimeToIntermediate += (d.arrivedAtIntermediate - d.loadTime)
                timeAtIntermediate += (d.leftIntermediate - d.arrivedAtIntermediate)
                transitTimeToTarget += (d.endTime - d.leftIntermediate)

                # f1.write("%d;%s;%d;%d;%d\n" % (d.startTime, target, (d.loadTime - d.startTime), (d.leftIntermediate - d.arrivedAtIntermediate), (d.endTime - d.startTime)))
                i += 1
            # f1.write("\n")
            if dataLength > 0:
                if ARRIVAL_TYPE == "infinite":
                    line = "%s;%g;%g;%g;%g;%g;%g\n" % (to_target, gamma_S, gamma_I, split_ratio, alpha, dataLength, (dataLength / SIM_TIME))
                    f1.write(line)
                    print line
                elif ARRIVAL_TYPE == "Poisson":
                    line = "%s;%g;%g;%g;%g;%g;%g;%g;%g;%g;%g\n" % (to_target, gamma_S, gamma_I, split_ratio, alpha, dataLength, (dataLength / SIM_TIME), DATA_RATE[target], beta, rate_1, (transitTime/dataLength))
                    f1.write(line)
                    print line

    traci.close()
    sys.stdout.flush()
    f.close()

def get_options():
    optParser = optparse.OptionParser()
    optParser.add_option("--nogui", action="store_true", default=False, help="run the commandline version of sumo")
    options, args = optParser.parse_args()
    return options


# this is the main entry point of this script
if __name__ == "__main__":
    sumoBinary = checkBinary('sumo')

    generate_routefile()

    # this is the normal way of using traci. sumo is started as a
    # subprocess and then the python script connects and runs
    sumoProcess = subprocess.Popen([sumoBinary, "-c", "sim.sumo.cfg", "--remote-port", str(PORT)], stdout=sys.stdout, stderr=sys.stderr)
    run()
    sumoProcess.wait()
