library(tools)
library(reshape2)
library(ggplot2)

options(echo=TRUE) # if you want see commands in output file
args <- commandArgs(trailingOnly = TRUE)
print(args)
print(args[1])

input_file = args[1]
output_file = paste(file_path_sans_ext(args[1]), "pdf", sep=".")

pdf(output_file, width = 10, height = 3.5)

d <- read.table(input_file, sep=";", quote="\"")
d$grp <- paste(d$V1, d$V9)
dat.m = melt(d, id.var=c("V10", "grp"), measure.vars = c("V11"))
ggplot(dat.m, aes(x=V10, y=value, group=grp, colour=as.factor(grp))) + 
  geom_line() + 
  geom_point() + 
  xlab("Arrival rate of data 1 at the source") +   # Remove x-axis label
  ylab("Total delay (simulation timesteps)") + 
  guides(col = guide_legend(nrow = 3, title = "Target and split ratio",)) 
