import csv
import sys,os
import numpy

class AutoVivification(dict):
    """Implementation of perl's autovivification feature."""
    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value

if len(sys.argv) <= 1 or len(sys.argv) > 2:
	print "usage: python %s <result-file>" % sys.argv[0]
	sys.exit()

filename = sys.argv[1]
res = AutoVivification()
with open(filename, 'rb') as f:
    reader = csv.reader(f, delimiter=' ')
    for row in reader:
    	target = row[0]
    	alpha = float(row[1])
    	if alpha not in res[target]:
    		res[target][alpha] = {'tot_time':[], 'time_S':[], 'time_S_I':[], 'time_I':[], 'time_I_T':[]}
    	res[target][alpha]['tot_time'].append(float(row[2]))
    	res[target][alpha]['time_S'].append(float(row[3]))
    	res[target][alpha]['time_S_I'].append(float(row[4]))
    	res[target][alpha]['time_I'].append(float(row[5]))
    	res[target][alpha]['time_I_T'].append(float(row[6]))

path, ext = os.path.splitext(filename)
with open(path+'_mean'+ext, 'w') as f:
	for t in res.keys():
		f.write("Target;Alpha;Time at S;Time at S (std);Transit time S - I;Transit time S - I (std);Time at I;Time at I (std);Transit time I-T;Transit time I-T (std);Total transit time;Total transit time (std)\n")
		for alpha in res[t].keys():
			tot_time_m = numpy.mean(res[t][alpha]['tot_time'])
			tot_time_s = numpy.std(res[t][alpha]['tot_time'])
			time_S_m = numpy.mean(res[t][alpha]['time_S'])
			time_S_s = numpy.std(res[t][alpha]['time_S'])
			time_S_I_m = numpy.mean(res[t][alpha]['time_S_I'])
			time_S_I_s = numpy.std(res[t][alpha]['time_S_I'])
			time_I_m = numpy.mean(res[t][alpha]['time_I'])
			time_I_s = numpy.std(res[t][alpha]['time_I'])
			time_I_T_m = numpy.mean(res[t][alpha]['time_I_T'])
			time_I_T_s = numpy.std(res[t][alpha]['time_I_T'])
			f.write("%s;%g;%g;%g;%g;%g;%g;%g;%g;%g;%g;%g\n" % 
				(t, alpha, time_S_m, time_S_s, time_S_I_m, time_S_I_s, time_I_m, time_I_s, time_I_T_m, time_I_T_s, tot_time_m, tot_time_s))

