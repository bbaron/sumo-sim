##
#
# All functions related to the controller
#
##

import re
import cplex
from fractions import gcd
from cplex.exceptions import CplexSolverError

from classes import *
from tools import *

def apply_weights(G, paths_weights, demands, sch_type, full=True):
    """ Function that applies the weights decided in the flow allocation in the graph G """

    # Put the allocated flow for the paths in the demands
    for path_name in paths_weights.keys():
        match = re.search( r'(d[0-9]+)p([0-9]+)', path_name, re.M|re.I)
        demand = match.group(1)
        demands[demand].setPathFlow(path_name, paths_weights[path_name])

    # Apply the weights for the logical links
    for u,v in G.edges():
        for p in G[u][v]['flows']:
            if p[1] in paths_weights:
                p[2] = paths_weights[p[1]]
            else:
                p[2] = 0.0

    if sch_type in ('MCF-RR', 'MMF-RR', 'MCF', 'MMF'):
        for u in G.nodes():
            if 'sch_data' in G.node[u]:
                for edge in G.node[u]['sch_data']:
                    e_demands = get_edge_attribute_from_name(G, edge, 'flows')
                    d_weights = {}
                    for d in e_demands:
                        if d[0] not in d_weights:
                            d_weights[d[0]] = 0.0
                        d_weights[d[0]] += d[2]
                    # Fill the rest of the capacity with None
                    if not full:
                        d_weights[None] = 1.0 - sum(w for w in d_weights.values())

                    if __debug__:
                        print u, edge, d_weights

                    if sch_type in ('MCF', 'MMF'):
                        G.node[u]['sch_data'][edge]['sch_seq'] = weighted_random_generator(d_weights)
                    if sch_type in ('MCF-RR', 'MMF-RR'):
                        G.node[u]['sch_data'][edge]['sch_seq'] = round_robin_seq(d_weights)
                    G.node[u]['sch_data'][edge]['sch_seq_len'] = len([d for (d,v) in d_weights.items() if v > 0.0])



def maxMinFairnessMCF(G,C,U,Z, weights, arrival_type, demands, retransmission='local'):
    """ Max Min Fairness linear program 
         - G is the overlay graph
         - C is the list of all the commodity indexes (equivalent to demands)
         - U is the list of all the commodity indexes (demands) with unallocated demands (initially equal to C)
         - Z is the dict of all the commodity indexes, indexed by i, that have been allocated at step i
    """

    # Create a new (empty) model and populate it below.
    model = cplex.Cplex()
    model.set_results_stream(None)
    model.set_log_stream(None)
    model.set_error_stream(None)
    model.set_warning_stream(None)

    # Add the variables in the model. We initialize with the first variable "t" 
    obj   = [1.0]              # Linear objective coefficients of the variables -- must be float
    names = ["t"]              # List of strings (variable names)
    lb    = [0.0]              # List of lower bounds (for the variables)
    ub    = [cplex.infinity]   # List of upper bounds (for the variables)
    
    # Add the commodities to the name variable and objective arrays
    # Other variables are ignored in the objective
    for demandIdx, demandData in demands.items():
        for path_name in demandData.getPaths().keys():
            # Variable name: d<demand_index>p<path_index>
            names.append(path_name)
            obj.append(0.0)
            lb.append(0.0)
            ub.append(cplex.infinity)

    # Do not forget to effectively add the variable to the model
    model.variables.add(obj   = obj,       # Objectve factor
                        lb    = lb,        # Lower bounds
                        ub    = ub,        # Upper bounds
                        names = names);    # Variables name

    # Add the constraints in the model
    # capacity_constraint => lin_expr may be either a list of SparsePair instances or a matrix in list-of-lists format
    # senses              => senses must be either a list of single-character strings or a string containing the senses of the linear constraints. Each entry must be one of 'G', 'L', 'E', and 'R', respectively indicating greater-than, less-than, equality, and ranged constraints
    # rhs                 => rhs is a list of floats, specifying the righthand side of each linear constraint

    # Flow constraints of each demand of C (all demands) on the flow
    if arrival_type != 'infinite':
        for demandIdx in C:
            weight = weights[demandIdx]
            demand_constraints = [] 
            demand_senses      = []
            demand_rhs         = []
            # cplex.SparsePair does the sum: sum(value * index)
            index = [] # Variables
            value = [] # Variables' factors 

            for path_name in demands[demandIdx].getPaths().keys():
                index.append(path_name)
                value.append(1.0)
            if index:
                demand_constraints.append(cplex.SparsePair(ind = index, val = value))
                demand_senses.append('L')
                demand_rhs.append(weight)

            model.linear_constraints.add(lin_expr = demand_constraints,
                                         senses   = demand_senses,
                                         rhs      = demand_rhs)
    # Capacity constraints (for all edges of the network)
    capacity_constraints = []
    capacity_senses      = []
    capacity_rhs         = []

    for u,v,d in G.edges(data=True):
        index = []
        value = []
        for (demand,path_name,weight) in d['flows']:
            index.append(path_name)
            if retransmission == 'local':
                value.append(d['retransmissions'])
            elif retransmission == 'global':
                value.append(demands[demand].getPathRet(path_name))

        if index:
            capacity_constraints.append(cplex.SparsePair(ind = index, val = value))
            capacity_senses.append('L')
            capacity_rhs.append(d['traffic'])

    model.linear_constraints.add(lin_expr = list(capacity_constraints),
                                 senses   = list(capacity_senses),
                                 rhs      = list(capacity_rhs))

    # Minimum allocation constraint for all commodities that have not been allocated yet (belonging to set U)
    for demandIdx in U:
        constraint        = [] 
        senses            = []
        rhs               = []
        constraints_names = []
        # cplex.SparsePair does the sum: sum(value * index)
        index = ["t"] # Variables
        value = [-1.0] # Variables' factors 
        for path_name in demands[demandIdx].getPaths().keys():
            index.append(path_name)
            value.append(1.0)  
        if index:
            constraint.append(cplex.SparsePair(ind = index, val = value))
            senses.append('G')
            rhs.append(0.0)
            constraints_names.append("c"+str(demandIdx))
       
        model.linear_constraints.add(lin_expr = constraint,
                                     senses   = senses,
                                     rhs      = rhs,
                                     names    = constraints_names)

    # Minimum allocation for all commodities that have been previously allocated (belonging to Z)
    # Z = [step][demandIdx]
    for step in Z.keys():
        for demandIdx in Z[step].keys():
            constraint = [] 
            senses     = []
            rhs        = []
            # cplex.SparsePair does the sum: sum(value * index)
            index = [] # Variables
            value = [] # Variables' factors 
            for path_name in demands[demandIdx].getPaths().keys():
                index.append(path_name)
                value.append(1.0)  
            if index:
                constraint.append(cplex.SparsePair(ind = index, val = value))
                senses.append('G')
                rhs.append(Z[step][demandIdx])
           
            model.linear_constraints.add(lin_expr = constraint,
                                         senses   = senses,
                                         rhs      = rhs);

    # Our objective is to Maximize the minimum allocation
    model.objective.set_sense(model.objective.sense.maximize)
    
    return names, model


def nonBlockingTestMCF(G,C,U,Z,demandIndex,tk,weights,arrival_type, demands, retransmission='local'):
    """ Non-blocking test for the current state of the max-min fairness algorithm
         - C is the list of all the commodity indexes (equivalent to demands)
         - U is the list of all the commodity indexes (demands) with unallocated demands (initially equal to C)
         - Z is the dict of all the commodity indexes, indexed by i, that have been allocated at step i
    """

    # Create a new (empty) model and populate it below.
    model = cplex.Cplex()
    model.set_results_stream(None)
    model.set_log_stream(None)
    model.set_error_stream(None)
    model.set_warning_stream(None)

    # Add the variables in the model
    # Add the commodities of the examined demandIndex to the name variable and objective arrays
    obj = []    # Linear objective coefficients of the variables
    names = []  # List of strings (variable names)
    lb = []     # List of lower bounds (for the variables)
    ub = []     # List of upper bounds (for the variables)

    demandData = demands[demandIndex]
    for path_name in demandData.getPaths().keys():
        names.append(path_name)
        obj.append(1.0)
        lb.append(0.0)
        ub.append(cplex.infinity)

    # Add the extra variables
    for demandIdx, demandData in demands.items():
        if demandIdx == demandIndex:
            continue
        for path_name in demandData.getPaths().keys():
            # Variable name: d<demand_index>p<path_index>
            names.append(path_name)
            obj.append(0.0)
            lb.append(0.0)
            ub.append(cplex.infinity)

    # Do not forget to effectively add the variable to the model
    model.variables.add(obj   = obj,       # Objectve factor
                        lb    = lb,        # Lower bounds
                        ub    = ub,        # Upper bounds
                        names = names);    # Variables name


    # Add the constraints in the model
    # capacity_constraint => lin_expr may be either a list of SparsePair instances or a matrix in list-of-lists format
    # senses              => senses must be either a list of single-character strings or a string containing the senses of the linear constraints. Each entry must be one of 'G', 'L', 'E', and 'R', respectively indicating greater-than, less-than, equality, and ranged constraints
    # rhs                 => rhs is a list of floats, specifying the righthand side of each linear constraint

    # Flow constraints of each demand of C (all demands) on the flow
    if arrival_type != 'infinite':
        for demandIdx in C:
            weight = weights[demandIdx]
            demand_constraints = [] 
            demand_senses      = []
            demand_rhs         = []
            # cplex.SparsePair does the sum: sum(value * index)
            index = [] # Variables
            value = [] # Variables' factors 

            for path_name in demands[demandIdx].getPaths().keys():
                index.append(path_name)
                value.append(1.0)
            if index:
                demand_constraints.append(cplex.SparsePair(ind = index, val = value))
                demand_senses.append('L')
                demand_rhs.append(weight)

            model.linear_constraints.add(lin_expr = demand_constraints,
                                         senses   = demand_senses,
                                         rhs      = demand_rhs)

    # Capacity constraints
    capacity_constraints = []
    capacity_senses      = []
    capacity_rhs         = []

    for u,v,d in G.edges(data=True):
        index = []
        value = []
        for (demand,path_name,weight) in d['flows']:
            index.append(path_name)
            if retransmission == 'local':
                value.append(d['retransmissions'])
            elif retransmission == 'global':
                value.append(demands[demand].getPathRet(path_name))

        if index:
            capacity_constraints.append(cplex.SparsePair(ind = index, val = value))
            capacity_senses.append('L')
            capacity_rhs.append(d['traffic'])

    model.linear_constraints.add(lin_expr = list(capacity_constraints),
                                 senses   = list(capacity_senses),
                                 rhs      = list(capacity_rhs))


    # Minimum allocation for all commodities that have been previously allocated (belonging to Z)
    # Z = [step][demandIdx]
    for step in Z.keys():
        for demandIdx in Z[step].keys():
            constraint = [] 
            senses     = []
            rhs        = []
            # cplex.SparsePair does the sum: sum(value * index)
            index = [] # Variables
            value = [] # Variables' factors 
            for path_name in demands[demandIdx].getPaths().keys():
                index.append(path_name)
                value.append(1.0)  
            if index:
                constraint.append(cplex.SparsePair(ind = index, val = value))
                senses.append('G')
                rhs.append(Z[step][demandIdx])
           
            model.linear_constraints.add(lin_expr = constraint,
                                         senses   = senses,
                                         rhs      = rhs)

    # Minimum allocation constraint for all commodities that have not been allocated yet (belonging to set U)
    for demandIdx in U:
        constraint        = [] 
        senses            = []
        rhs               = []
        # cplex.SparsePair does the sum: sum(value * index)
        index = [] # Variables
        value = [] # Variables' factors 
        for path_name in demands[demandIdx].getPaths().keys():
            index.append(path_name)
            value.append(1.0)  
        if index:
            constraint.append(cplex.SparsePair(ind = index, val = value))
            senses.append('G')
            rhs.append(tk)
       
        model.linear_constraints.add(lin_expr = constraint,
                                     senses   = senses,
                                     rhs      = rhs)

    # Our objective is to Maximize the minimum allocation
    model.objective.set_sense(model.objective.sense.maximize)
    
    return names, model


def maxMinFairness(G, weights, arrival_type, demands, sch_type, retransmission='local'):
    # Initialization :
    C = demands.keys()      # Set of all commodities
    U = demands.keys()      # Set of all commodites whose allocation has not been fixed yet
    Z = AutoVivification()  # Dictionary of commodities whose allocation has been fixed at step i (dict indexed by the step indexes)
    F = {}                  # Resulting allocated flows 
    k = 0                   # Step counter

    while U:
        # Maximize the ith smallest allocation by solving max_min_fairness_linear_program
        names, model = maxMinFairnessMCF(G, C, U, Z, weights, arrival_type, demands, retransmission)
        try:
            model.solve()
        except CplexSolverError, e:
            print "Exception raised during solve: " + e
            return
        
        solution = model.solution

        # Get the minimum allocation value tk for step tk -- The value is for the first variable "t" of the list names
        tk = solution.get_values(0)

        # Put the results in a dictionary
        results = AutoVivification()
        for i in range(1,len(names)):
            # We consider all other flow variables (other than variable "t") that have been allocated 
            if solution.get_values(i) > 0.0:
                match  = re.search( r'(d[0-9]+)p([0-9]+)', names[i], re.M|re.I)
                demand = match.group(1)
                path   = names[i]
                
                results[demand][path] = solution.get_values(i)

        demandsToAllocate = []

        # Allocate the demands to be allocated 
        # and determine those that can be increased
        for demandIndex in U:
            sumFlow = sum([flow for flow in results[demandIndex].values()])
            
            if __debug__:
                print k, demandIndex, tk, sumFlow, demands[demandIndex].getRate()

            # If the demand has been satisfied
            if arrival_type != 'infinite' and feq(sumFlow, demands[demandIndex].getRate()) or (sumFlow >= demands[demandIndex].getRate()):
                # The demand must be allocated
                demandsToAllocate.append(demandIndex)
                # continue to the next demand
                continue

            # If the allocated volume is greater than tk
            if not feq(sumFlow, tk) and sumFlow > tk:
                # The examined demand can be further increased
                # continue to the next demand
                continue

            # Run the non-blocking test linear progam for the current demand
            names, model = nonBlockingTestMCF(G, C, U, Z, demandIndex, tk, weights,arrival_type, demands, retransmission)
            try:
                model.solve()
            except CplexSolverError, e:
                print "Exception raised during solve: " + e
                return

            solution = model.solution

            # Compute the allocation solution for the demand
            sumDemand_NBT = 0.0
            for i in range(len(names)):
                match  = re.search( r'(d[0-9]+)p([0-9]+)', names[i], re.M|re.I)
                demand = match.group(1)
                if demand != demandIndex:
                    continue
                
                if __debug__:
                    print "\t", names[i], demand, demandIndex, solution.get_values(i)

                # Increment the sum for the demand
                sumDemand_NBT += solution.get_values(i)

            if feq(sumDemand_NBT,tk) or (sumDemand_NBT <= tk):
                # The demand can be allocated, as it cannot be further increased
                demandsToAllocate.append(demandIndex)

        if __debug__:
            print k, tk, results, U, demandsToAllocate

        # When all the demands in U have been examined, modify the sets
        for demandIndex in demandsToAllocate:
            Z[k][demandIndex] = tk  # Fix the demand to tk
            U.remove(demandIndex)   # the demand has been allocated
            # Also add the commodity to the resulting dictionary, as the flow is satisfied
            F[demandIndex] = results[demandIndex]

        # Increment the step counter
        k += 1

    res = {}
    for demand, flows in F.items():
        for path_name, flow in flows.items():
            res[path_name] = flow

    print dict([(p,f) for p,f in res.items() if f > 0])

    apply_weights(G, res, demands, sch_type)

    return res


def multiCommodityFlow(G, weights, arrival_type, demands, sch_type, retransmission='local'):
    model = cplex.Cplex()
    model.set_results_stream(None)
    model.set_log_stream(None)
    model.set_error_stream(None)
    model.set_warning_stream(None)

    # Add the variables in the model
    obj   = [] # Linear objective coefficients of the variables
    names = [] # List of strings (variable names)
    lb    = [] # List of lower bounds (for the variables)
    ub    = [] # List of upper bounds (fro the variables)

    for d in demands.values():
        for path_name in d.getPaths().keys():
            obj.append(1.0)
            names.append(path_name)
            lb.append(0.0)
            ub.append(cplex.infinity)

    model.variables.add(obj = obj,          # Objectve factor
                        lb = lb,            # Lower bounds
                        ub = ub,            # Upper bounds
                        names = names)      # Variables name

    # Capacity constraints
    capacity_constraints = []
    capacity_senses      = []
    capacity_rhs         = []

    for u,v,d in G.edges(data=True):
        index = []
        value = []
        for (demand,path_name,weight) in d['flows']:
            index.append(path_name)
            if retransmission == 'local':
                value.append(d['retransmissions'])
            elif retransmission == 'global':
                value.append(demands[demand].getPathRet(path_name))

        if index:
            capacity_constraints.append(cplex.SparsePair(ind = index, val = value))
            capacity_senses.append('L')
            capacity_rhs.append(d['traffic'])

    model.linear_constraints.add(lin_expr = list(capacity_constraints),
                                 senses   = list(capacity_senses),
                                 rhs      = list(capacity_rhs))

    if arrival_type != 'infinite':
        # Demand constraints
        for demand,weight in weights.items():
            demand_constraints = []
            demand_senses      = []
            demand_rhs         = []
            # cplex.SparsePair does the sum: sum(value * index)
            index = [] # Variables
            value = [] # Variables' factors 
            for path_name in demands[demand].getPaths().keys():
                index.append(path_name)
                value.append(1.0)
            if index:
                demand_constraints.append(cplex.SparsePair(ind = index, val = value))
                demand_senses.append('L')
                demand_rhs.append(weight)

            model.linear_constraints.add(lin_expr = demand_constraints,
                                         senses   = demand_senses,
                                         rhs      = demand_rhs)

    # Our objective is to Maximize the flows.
    model.objective.set_sense(model.objective.sense.maximize)

    try:
        model.solve()
    except CplexSolverError, e:
        print "Exception raised during solve: " + e
    else:
        solution = model.solution

    # Put the results of the solution in the edges
    res = {}
    for i in range(len(names)):
        res[names[i]] = solution.get_values(i)
    print dict([(p,f) for p,f in res.items() if f > 0])
    
    apply_weights(G, res, demands, sch_type)

    return res
