import math
import scipy.spatial
import networkx as nx
import itertools
import random
import time
from shapely.geometry import LineString, MultiLineString
from operator import mul, itemgetter, attrgetter
from fractions import gcd

from classes import *

def get_closest_junction(G, station):
    """ Get the nearest nodes from the station location
        Uses the k nearest nodes algorithm
    """
    knn = scipy.spatial.cKDTree(G.nodes(), leafsize = 100)
    node = G.nodes()[knn.query(station, k = 1)[1]]
    return node

def get_closest_node_overlay(G, overlay, node):
    """ Get the nearest node on G from the node location
        Uses the k nearest nodes algorithm

    """
    nodes = [u for u,d in G.nodes(data=True) if d['name'] in overlay.nodes()]
    knn = scipy.spatial.cKDTree(nodes, leafsize = 100)
    u = nodes[knn.query(node, k = 1)[1]]
    return G.node[u]['name']

def get_edges(path):
    """ Returns the list of edges from a list of nodes (path)
    """
    return zip(path[0:], path[1:])

def feq(a,b):
    return abs(a - b) <= 0.0001

def round_float(x,n=2):
    return float('%.*f' % (n,round(x, n)))

def interleave_seq(lists):
    return [item[1] for item in 
        sorted(itertools.chain(*(
            zip(itertools.count(1.0/len(seq), 1.0/len(seq)), seq)
            for seq in lists if seq)))] 

def round_robin_seq(d_weights):
    """ Returns the round robin sequence from the demand_weights 
         - d_weights: {'di': w_i}
    """
    # Create the sequence for the weights
    weights = dict([(d,int(round_float(x)*100)) for (d,x) in d_weights.items()])
    div = reduce(gcd,weights.values())
    
    if div <= 0: 
        sequence = [None]
    else:
        lists = [[demand]*int(w/div) for (demand, w) in weights.items()]
        sequence = interleave_seq(lists)

    i = 0
    while True:
        yield sequence[i]
        i = (i+1) % len(sequence)

def weighted_random_generator(weights):
    """ weights are the weights indexed by the elements to return
        0.0 <= weights.get(i) <= 1.0 """
    table = [ z for x, y in weights.items() for z in [x]*int(round_float(y)*100) ]
    while True:
        if table:
            yield random.choice(table)
        else:
            yield None

def round_robin_generator(sequence):
    i = 0
    while True:
        yield sequence[i]
        i = (i+1) % len(sequence)


def commonality_factor(G, path_id, paths, beta=1, gamma=1, utility="travelTime"):
    """  - paths are the output of the alternative_routes function
    """
    ratioSum = 0.0
    for h in paths.values():
        sharedLength = path_shared_length_inv(G, h['path'], paths[path_id]['path'], weight=utility)
        h_length = sum([1/G[u][v][utility] for (u,v) in get_edges(h['path']) if G.has_edge(u,v) and G[u][v][utility] > 0])
        p_length = sum([1/G[u][v][utility] for (u,v) in get_edges(paths[path_id]['path']) if G.has_edge(u,v) and G[u][v][utility] > 0])
        ratioSum += (sharedLength / (h_length**(1/2) * p_length**(1/2)))**gamma

    return beta * math.log(ratioSum)

def select_edge(G, attr_name, attr_value):
    """ Returns the edge that features the given attribute (name,value) """
    return [(u,v) for u,v,d in G.edges_iter(data=True) if d[attr_name] == attr_value][0]

def select_node(G, attr_name, attr_value):
    """ Returns the node that features the given attribute (name,value) """
    return [u for u,d in G.nodes_iter(data=True) if d[attr_name] == attr_value][0]

def get_edge_attribute_from_name(G, edge, attr_name):
    """ Returns the attribute name of edge from its name """
    (u,v) = select_edge(G, 'name', edge)
    return G[u][v][attr_name]

def get_edges_in(G, node):
    """ Returns the list of the names of incoming edges at node """
    return [d['name'] for u,v,d in G.in_edges_iter(node, data=True)]

def get_edges_out(G, node):
    """ Returns the list of the names of outgoing edges at node """
    return [d['name'] for u,v,d in G.out_edges_iter(node, data=True)]

def get_node_from_edge(G, edge, where):
    """ Returns the node from the edge name and whether the vehicle is departing or arriving at the edge
         - where in enum('in', 'out') """
    (u,v) = select_edge(G, 'name', edge)
    if where == 'in': 
        return G.node[u]['name']
    elif where == 'out': 
        return G.node[v]['name']
    return None


def generateArrivals(demands, sim_time):
    """ Generate the list of Poisson arrivals for the two data streams
         - demands is the dictionary of demands """
    def arrivalGenerator(rate):
        t = 0
        while True:
            t += random.expovariate(rate)
            yield t

    t = [(i, 0) for i in demands.keys()]
    arrivals = []
    for i in demands.keys():
        t = 0
        if demands[i].getRate() > 0:
            generator = arrivalGenerator(demands[i].getRate())
            arrivals += [(i, arrival) \
                         for arrival in itertools.takewhile(lambda t: t < sim_time, generator)]

    sorted_arrivals = sorted(arrivals, key=lambda x: x[1])

    return sorted_arrivals

def path_shared_length(G, p1, p2, weight):
    """ Returns the length of the intersection between paths p1 and p2
        p1 and p2 must be expressed as a list of vertices
    """
    line1 = LineString(p1)
    line2 = LineString(p2)
    
    path_length = 0.0
    intersection = line1.intersection(line2)

    if intersection.geom_type is "LineString":
        path = list(intersection.coords)
        path_length = sum([G[u][v][weight] for (u,v) in get_edges(path) if G.has_edge(u,v) and G[u][v][weight] > 0])

    if intersection.geom_type is "MultiLineString":
        path = [tuple(edge.coords) for edge in intersection.geoms if G.has_edge(*tuple(edge.coords))]
        path_length = sum([G[u][v][weight] for (u,v) in path if G.has_edge(u,v) and G[u][v][weight] > 0])
    
    if intersection.geom_type is "GeometryCollection":
        for geom in intersection.geoms:
       
            if geom.geom_type is "LineString":
                for (u,v) in get_edges(list(geom.coords)):
                    if G.has_edge(u,v):
                        path_length += G[u][v].get(weight,1)
            if geom.geom_type is "MultiLineString":
                for line in geom.geoms:
                    for (u,v) in get_edges(list(line.coords)):
                        if G.has_edge(u,v):
                            path_length += G[u][v].get(weight,1)
    return path_length

def path_shared_length_inv(G, p1, p2, weight):
    """ Returns the length of the intersection between paths p1 and p2
        p1 and p2 must be expressed as a list of vertices
        we consider the inverse of the given weight on the edges
    """
    line1 = LineString(p1)
    line2 = LineString(p2)
    
    path_length = 0.0
    intersection = line1.intersection(line2)

    if intersection.geom_type is "LineString":
        path = list(intersection.coords)
        path_length = sum([1/G[u][v][weight] for (u,v) in get_edges(path) if G.has_edge(u,v) and G[u][v][weight] > 0])

    if intersection.geom_type is "MultiLineString":
        path = [tuple(edge.coords) for edge in intersection.geoms if G.has_edge(*tuple(edge.coords))]
        path_length = sum([1/G[u][v][weight] for (u,v) in path if G.has_edge(u,v) and G[u][v][weight] > 0])
    
    if intersection.geom_type is "GeometryCollection":
        for geom in intersection.geoms:
       
            if geom.geom_type is "LineString":
                for (u,v) in get_edges(list(geom.coords)):
                    if G.has_edge(u,v):
                        path_length += 1/max(G[u][v].get(weight,1), 1)
            if geom.geom_type is "MultiLineString":
                for line in geom.geoms:
                    for (u,v) in get_edges(list(line.coords)):
                        if G.has_edge(u,v):
                            path_length += 1/max(G[u][v].get(weight,1), 1)
    return path_length


def top_shortest_paths(G, source, target, weight='distance', weight2='travelTime', maxWeight=200, k=10, beta=1.2, maxSim=0.8):
    """ Compute the top shortest paths by iterating and ajusting the weight by a factor beta after each dijkstra shortest path computation
    """
    # First check that the optimal path is less than maxWeight
    (lOpt, Opt) = nx.single_source_dijkstra(G, source, target=target, weight=weight)
    if lOpt[target] > maxWeight:
        return []

    # We work on a copy of the graph, as we will modfy the weigths of the edges
    H = G.copy()
    resultPaths = AutoVivification()
    i = 0
    while i < k:
        (length, path) = nx.single_source_dijkstra(H, source, target=target, weight=weight)
        flag = True
        # Check whether the path is the same as the one calculated according to the maxSim ratio
        for testPath_id, testPath in resultPaths.items():
            # Get the shared length of the current tested path and the output path
            # In this case, we consider the original graph, with unchanged weights
            sharedLength = path_shared_length(G, testPath['path'], path[target], weight)
            if sharedLength > maxSim * testPath['pathLength']:
                # If the shared path length is greater than maxSim proportion of the tested path, 
                # do not save the output path
                flag = False
        
        if flag and length[target] <= maxWeight:
            # Save the results and increase the counter
            travelTime = sum([G[u][v][weight2] for (u,v) in get_edges(path[target]) if G.has_edge(u,v)])
            resultPaths[i]['path'] = path[target]
            resultPaths[i]['pathLength'] = length[target]
            resultPaths[i]['pathTime'] = travelTime
            i += 1
            print length[target], travelTime

        if length[target] > maxWeight:
            # Cannot have better paths than the ones already computed...
            break

        # In any case, modify the weigths of the graph H by a factor beta
        for (u,v) in get_edges(path[target]):
            H[u][v][weight] *= beta

    H.clear() # Free memory
    return resultPaths

def ksp_yen(G, node_start, node_end, max_k=2, weight='weight'):
    """ Return the  k-shortest paths between node_start and node_end, following the given weight.
        The function follows Yen's algorithm.
        Returns a dictionary indexed with keys 'cost' and 'path'. 
    """
    graph = G.copy()    # Deep copy of the graph to work on it
    distances, path = nx.single_source_dijkstra(graph, node_start, weight=weight)

    A = [{'cost': distances[node_end], 
          'path': path[node_end]}]
    B = []

    if not A[0]['path']: return A

    for k in range(1, int(max_k)):
        for i in range(0, len(A[-1]['path']) - 1):
            node_spur = A[-1]['path'][i]
            path_root = A[-1]['path'][:i+1]

            for path_k in A:
                curr_path = path_k['path']
                if len(curr_path) > i and path_root == curr_path[:i+1] and graph.has_edge(curr_path[i], curr_path[i+1]):
                    graph.remove_edge(curr_path[i], curr_path[i+1])
            
            distances_spur, path_spur = nx.single_source_dijkstra(graph, node_spur, node_end, weight=weight)

            if node_end in path_spur and path_spur[node_end]:
                path_total = path_root[:-1] + path_spur[node_end]
                dist_total = distances[node_spur] + distances_spur[node_end]
                potential_k = {'cost': dist_total, 'path': path_total}

                if not (potential_k in B):
                    B.append(potential_k)
        if len(B):
            B = sorted(B, key=itemgetter('cost'))
            A.append(B[0])
            B.pop(0)
        else:
            break

    return A

