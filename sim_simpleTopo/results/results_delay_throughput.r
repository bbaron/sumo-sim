library(tools)
library(reshape2)
library(ggplot2)

options(echo=TRUE) # if you want see commands in output file
args <- commandArgs(trailingOnly = TRUE)
print(args)
print(args[1])

input_file = args[1]
results <- read.table(input_file, sep=";", quote="\"")
results$grp <- paste(results$V1, results$V2)

plot_type = args[2]
output_file = paste(paste(file_path_sans_ext(args[1]), args[2], sep="-"), "pdf", sep=".")
pdf(output_file, width = 15, height = 3.5)

if (plot_type == 'delay') {
	ggplot(results, aes(x=V4, y=V6, fill=V6)) + geom_bar(stat = 'identity', position = 'stack')+ facet_grid(~ grp)+ xlab("Configuration") + ylab("Delay (simulation timestep)")
} else if (plot_type == 'throughput') {
	ggplot(results, aes(x=V4, y=V7, fill=V7)) + geom_bar(stat = 'identity', position = 'stack')	+ facet_grid(~ grp)	+ xlab("Configuration") + ylab("Throughput (data chunk/second)")
}


