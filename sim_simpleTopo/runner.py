#!/usr/bin/env python
from __future__ import division
import os, sys, getopt
import optparse
import subprocess
import itertools
from operator import mul, itemgetter, attrgetter
from collections import Counter, deque
from fractions import gcd
import math
import re
import numpy as np
import networkx as nx
import cplex
from cplex.exceptions import CplexSolverError

# we need to import python modules from the $SUMO_HOME/tools directory
try:
    sys.path.append(os.path.join(os.environ.get("SUMO_HOME"), "tools")) 
    sys.path.append(os.path.join(os.path.dirname('__file__'), '..'))
    from sumolib import checkBinary
except ImportError:
    sys.exit("please declare environment variable 'SUMO_HOME' as the root directory of your sumo installation (it should contain folders 'bin', 'tools' and 'docs')")

import traci
from classes import *
from tools import *
from schedulers import *
from controller import *
from settings import *


def init():
    global NET, DEMANDS, DEMAND_WEIGHTS, SOURCES, TARGETS, LOCATIONS, ROAD_NETWORK
    """ Initilization function """

    DEMANDS = { 
        'd1': Demand('d1', 'S1', 'D1', 0.5),
        'd2': Demand('d2', 'S1', 'D2', 0.5),
        'd3': Demand('d3', 'S2', 'D2', 0.5)
    } # Demand: (source, target, rate/size of data, demand paths {path name:path})

    if ARRIVAL_TYPE == 'fixed':
        sum_demand_weights = sum([d.getRate() for d in DEMANDS.values()])
        DEMAND_WEIGHTS = dict([(demand, d.getRate()/sum_demand_weights) for (demand,d) in DEMANDS.items()])
    elif ARRIVAL_TYPE == 'infinite':
        DEMAND_WEIGHTS = dict([(demand, d.getRate()) for (demand,d) in DEMANDS.items()])

    SOURCES = set([d.getOrigin() for d in DEMANDS.values()])
    TARGETS = set([d.getTarget() for d in DEMANDS.values()])

    NET.add_edge('S1', 'I1', name='e1', traffic=1, weight=1, flows=[])
    NET.add_edge('S2', 'I1', name='e2', traffic=1, weight=1, flows=[])
    NET.add_edge('I1', 'I2', name='e3', traffic=1, weight=1, flows=[])
    NET.add_edge('I1', 'I3', name='e4', traffic=1, weight=1, flows=[])
    NET.add_edge('I2', 'I3', name='e5', traffic=1, weight=1, flows=[])
    NET.add_edge('I2', 'D1', name='e6', traffic=1, weight=1, flows=[])
    NET.add_edge('I3', 'D2', name='e7', traffic=1, weight=1, flows=[])

    ROAD_NETWORK = NET
    for node in ROAD_NETWORK.nodes():
        ROAD_NETWORK.node[node]['name'] = node

    # Initialize the data at the offloading spots
    if ARRIVAL_TYPE in ['infinite', 'fixed']:
        for node in SOURCES:
            NET.node[node]['data'] = dict([(demand, sum([[Data(demand, seq=DEMANDS[demand].incCounter())] for i in range(5)], [])) for demand,d in DEMANDS.items() if d.getOrigin() == node])

    # Initialize the paths at the edges
    for (demand, d) in DEMANDS.items():
        # Compute the k-shortest paths between src (d[0]) and target (d[1])
        path_id = 1
        for ksp_data in ksp_yen(NET, d.getOrigin(), d.getTarget(), max_k=MAX_LOGICAL_PATHS):
            (t,p) = ksp_data['cost'], ksp_data['path']
            
            path_name = demand+"p"+str(path_id)
            d.addPath(path_name, p)
            for (u,v) in get_edges(p):
                # As follows: (demand name, path name (for the demand), weight for the path)
                NET[u][v]['flows'].append([demand, path_name, 1.0])
                if 'data' not in NET.node[u]:
                    NET.node[u]['data'] = {}
                if 'expected_data' not in NET.node[u]:
                    NET.node[u]['expected_data'] = {}
                if 'data' not in NET.node[v]:
                    NET.node[v]['data'] = {}
                if 'expected_data' not in NET.node[v]:
                    NET.node[v]['expected_data'] = {}
                if demand not in NET.node[u]['data']:
                    NET.node[u]['data'][demand] = []
                if demand not in NET.node[v]['data']:
                    NET.node[v]['data'][demand] = []

                if not 'sch_data' in NET.node[u]:
                    NET.node[u]['sch_data'] = {}
                NET.node[u]['sch_data'][NET[u][v]['name']] = {'cur': 0} # if round robin scheduler (index increment)
            path_id += 1

    # Initialize the round robin
    if SCHEDULER_TYPE in ('MCF-RR', 'MMF-RR'):
        for u in NET.nodes():
            if 'sch_data' in NET.node[u]:
                for edge in NET.node[u]['sch_data'].keys():
                    NET.node[u]['sch_data'][edge] = {
                        'sch_seq': [],                 # Periodic sequence of demands
                        'sch_stack': deque(maxlen=10), # Stack for sequence indexes that have not been allocated
                        'cur': 0                       # Current index of the sequence
                    }

def regenerate_data_at_source(src):
    global NET

    if ARRIVAL_TYPE == 'infinite' and src not in SOURCES:
        return
    for demand in NET.node[src]['data'].keys():
        if len(NET.node[src]['data'][demand]) == 0:
            # Add more data to the source storage
            NET.node[src]['data'][demand] += [Data(demand)]*100

def generate_routefile():
    """ This function generates the route file from the attributes given in the command line """

    with open("sim.rou.xml", "w") as routes:
        print >> routes, """<routes>
    <!-- Types of vehicles -->
    <vTypeDistribution id="typedist1">"""
        print >> routes, '        <vType id="type1" accel="0.8" vClass="passenger"  length="5" maxSpeed="70" probability="%.2f" color="0,0,1" />' % (1.0-PENETRATION_RATIO)
        print >> routes, '        <vType id="type2" accel="0.8" vClass="passenger"  length="5" maxSpeed="70" probability="%.2f" color="0,1,1" />' % (PENETRATION_RATIO)
        print >> routes, "    </vTypeDistribution>"

        print >> routes, '    <!-- Flows -->'
        flow_sum = 0
        for (u,v) in NET.edges():
            traffic = NET[u][v]['traffic']
            if traffic > 0:
                print >> routes, '    <flow id="flow%d" departPos="0" departLane="random" departSpeed="max" type="typedist1" begin="0.00" vehsPerHour="%.2f">' % (flow_sum, traffic*3600)
                print >> routes, '        <route edges="%s"/>' % (NET[u][v]['name'])
                print >> routes, '    </flow>'
            flow_sum += 1
        print >> routes, '</routes>'


def checkVehType(vehID):
    # Continue if the vehicle is not of type1 (the vehicles that are equipped with storage devices)
    if traci.vehicle.getTypeID(vehID) == "type1":
        return True 
    else: 
        return False

def sharesGPSInfo():
    return True # bool(random.getrandbits(1))

def dataInTransit():
    """ Return the amount of data in transit (ie. carried by vehicles) """
    return len([veh for veh in VEHICLES.values() if veh.getData()])

def execScheduler(G,node,out_edge):
    if SCHEDULER_TYPE == 'RR':
        return roundRobinScheduler(G,node,out_edge)
    elif SCHEDULER_TYPE == 'BUF':
        return bufferWeightedScheduler(G,node, out_edge)
    elif SCHEDULER_TYPE in ('MCF', 'MMF'):
        return weightedScheduler_full(G,node, out_edge)
    elif SCHEDULER_TYPE in ('MCF-RR', 'MMF-RR'):
        return weightedRoundRobinScheduler(G,node, out_edge)

def in_offloading_spot(node, vehID, step):
    """ Vehicle vehID just arrived at offloading spot node at step """
    global VEHICLES, NET, DATA_IN_TRANSIT

    # If the arrival type is infinite, regenerate some data
    if ARRIVAL_TYPE == 'infinite' and node in SOURCES:
        regenerate_data_at_source(node)

    veh = VEHICLES[vehID]
    if veh.isSharingGPSInfo():
        # We have access to the vehicle's GPS information
        # Get the vehicle destination (next-hop logical node)
        out_edge = veh.getLogicalLink()
        # Pass the sequence to the scheduler to get the data to load
        demand = execScheduler(NET, node, out_edge)

        if not demand: # Do nothing
            return

        # Load the data on the vehicle
        if len(NET.node[node]['data'][demand]) > 0:
            data = NET.node[node]['data'][demand].pop()
            if node in SOURCES:
                data.loadTime = step
            else:
                data.endStop(node, step)

            veh.loadData(data)
            DATA_IN_TRANSIT

            if __debug__:
                print "[%d] Vehicle %s arrived at node %s takes data %s" % (step, vehID, node, data)

    else:
        pass


def out_offloading_spot(node, vehID, step):
    """ Vehicle vehID just left at offloading spot node at step """
    global VEHICLES, NET

    veh = VEHICLES[vehID]

    # data drop-off
    data = veh.unloadData()
    if data:
        if node in TARGETS:
            # Check whether the data has arrived at the right destination
            if data.getDemand() not in NET.node[node]['data'].keys():
                print "Forwarding error"
                return

            data.endTime = step
        else:
            data.startStop(node, step)
        NET.node[node]['data'][data.getDemand()].append(data)
        if __debug__:
            print "[%d] Vehicle %s left at node %s brought data %s" % (step, vehID, node, data)


def run(port=PORT):
    """ execute the TraCI control loop
        -  """

    global VEHICLES, NET
    
    traci.init(port)

    if ARRIVAL_TYPE == 'Poisson':
        arrivals = generateArrivals(DEMANDS, SIM_TIME)

    step = 0
    print_count = 0
    output_file = 'results/I1_storage_%s_%s.txt' % (SCHEDULER_TYPE, ARRIVAL_TYPE)
    f = open(output_file, 'w')
    f.write("step;%s\n" % (';'.join([d for d in NET.node['I1']['data'].keys()])))
    while step < SIM_TIME:
        if print_count == 1000:
            print "[%s]" % step
            print_count = 0

        # traci.simulation.getMinExpectedNumber()
        traci.simulationStep()

        if ARRIVAL_TYPE == 'Poisson' :
            # Generate the data in the corresponding buffer according to a Poisson process
            while (len(arrivals) > 0) and (math.floor(arrivals[0][1]) == step):
                d = arrivals.pop(0)
                src = DEMANDS[d[0]].getOrigin()
                NET.node[src]['data'][d[0]].append(Data(d[0], startTime = step))

        # Get the vehicles ids that entered the simulation ("departed")
        enteredSim = traci.simulation.getDepartedIDList()
        for vehID in enteredSim:
            if not checkVehType(vehID):
                continue

            # Add the vehicle to the VEHICLES dictionary
            vehRoute = traci.vehicle.getRoute(vehID)
            vehOrigin = ROAD_NETWORK.node[select_edge(ROAD_NETWORK, 'name', vehRoute[0])[0]]['name']
            vehTarget = ROAD_NETWORK.node[select_edge(ROAD_NETWORK, 'name', vehRoute[-1])[1]]['name']
            vehLogicalLink = NET[vehOrigin][vehTarget]['name']

            if not vehID in VEHICLES:
                VEHICLES[vehID] = Vehicle(vehID, vehOrigin, vehTarget, vehLogicalLink)

            # Load the data at the offloading spot
            in_offloading_spot(vehOrigin, vehID, step)


        # Get the vehicles ids that exited the simulation ("arrived")
        exitedSim = traci.simulation.getArrivedIDList()
        for vehID in exitedSim:
            if vehID not in VEHICLES:
                continue
            
            veh = VEHICLES[vehID]
            if not veh.hasData(): 
                continue
            
            # Unload the data at the offloading spot
            out_offloading_spot(veh.getTarget(), vehID, step)
            # Delete the vehicle from the VEHICLES dictionary
            del VEHICLES[vehID]

        # Print STATS
        if __debug__:
            nodes = ['S1', 'S2', 'I1', 'I2', 'I3', 'D1', 'D2']
            for demand in DEMANDS.keys():
                print "%s: %s" % (demand, " ".join("%s %s " % tup for tup in [(node, len(NET.node[node]['data'][demand])) for node in nodes if demand in NET.node[node]['data']]))
            # for node, demand in [(node, [(demand, len(data)) for demand,data in d['data'].items()]) for node, d in NET.nodes(data=True)]:
            #     print "%s: %s" % (node, " ".join("(%s,%s)" % tup for tup in demand))

        f.write("%s;%s\n" % (step,';'.join([str(len(NET.node['I1']['data'][d])) for d in NET.node['I1']['data'].keys()])))

        step += 1
        print_count += 1

        if ARRIVAL_TYPE == 'fixed':
            if sum([DEMANDS[demand][2] == len(NET.node[d[1]]['data'][demand]) for (demand, d) in DEMANDS.items()]) == len(DEMANDS):
                break

    # Compute statistics on the data arrived at destination
    for target in TARGETS:
        for demand in NET.node[target]['data'].keys():
            sumTimes = 0
            dataLength = len(NET.node[target]['data'][demand])
            for data in NET.node[target]['data'][demand]:
                if data.loadTime < WARMUP_TIME:
                    continue
                sumTimes += (data.endTime - data.loadTime)
            if dataLength > 0:
                print "%s;%s;%s;%g;%g;%g" % (SCHEDULER_TYPE, ARRIVAL_TYPE, demand, dataLength, sumTimes/dataLength, dataLength / (step))
            else:
                print "%s;%s;%s;%g;%g;%g" % (SCHEDULER_TYPE, ARRIVAL_TYPE, demand, dataLength, 0, dataLength / (step))

    traci.close()
    f.close()
    sys.stdout.flush()

def get_options():
    optParser = optparse.OptionParser()
    optParser.add_option("--nogui", action="store_true", default=False, help="run the commandline version of sumo")
    optParser.add_option("-s", action="store", dest='scheduler', help="define the scheduler")
    optParser.add_option("-a", action="store", dest='arrival', help='define the data arrival type')
    options, args = optParser.parse_args()
    return options

def print_net_stats():
    for n in NET.nodes():
        print '## %s: [ %s]' % (n,' '.join("%s %s " % tup for tup in [(d,len(data)) for d,data in NET.node[n]['data'].items()]))
        if 'sch_data' in NET.node[n]:
            for e in NET.node[n]['sch_data'].keys():
                print '\t', e, NET.node[n]['sch_data'][e]
    print
    for u,v in NET.edges():
        print '## %s (traffic:%s, weight:%s)' % (NET[u][v]['name'], NET[u][v]['traffic'], NET[u][v]['weight'])
        for p in NET[u][v]['flows']:
            print '\t', p

def run_sumo(port):
    sumoBinary = checkBinary('sumo')
    sumoProcess = subprocess.Popen([sumoBinary, "-c", "sim.sumo.cfg", "--remote-port", str(port)], stdout=sys.stdout, stderr=sys.stderr)
    traci.init(port)
    print "process running", port
    # Get output of run
    traci.close()
    sumoProcess.wait()

# this is the main entry point of this script
if __name__ == "__main__":
    sumoBinary = checkBinary('sumo')
    options = get_options()

    # ports = [PORT+i for i in range(10)]
    # pool = Pool(processes=NB_PROCESSES)
    # res = pool.map(run_sumo, ports)

    if options.scheduler in SCHEDULERS:
        SCHEDULER_TYPE = options.scheduler
    if options.arrival in ARRIVALS:
        ARRIVAL_TYPE = options.arrival

    print "%s / %s" % (SCHEDULER_TYPE, ARRIVAL_TYPE)
    init()
    generate_routefile()

    if SCHEDULER_TYPE in ['MCF', 'MCF-RR']:
        multiCommodityFlow(NET, DEMAND_WEIGHTS, ARRIVAL_TYPE, DEMANDS)
    if SCHEDULER_TYPE in ['MMF', 'MMF-RR']:
        maxMinFairness(NET, DEMAND_WEIGHTS, ARRIVAL_TYPE, DEMANDS)

    if __debug__: print_net_stats()

    # this is the normal way of using traci. sumo is started as a
    # subprocess and then the python script connects and runs
    sumoProcess = subprocess.Popen([sumoBinary, "-c", "sim.sumo.cfg", "--remote-port", str(PORT)], stdout=sys.stdout, stderr=sys.stderr)
    run()
    sumoProcess.wait()
