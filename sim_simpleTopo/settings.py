import os, sys
import networkx as nx

sys.path.append(os.path.join(os.path.dirname('__file__'), '..'))
from classes import *

# the port used for communicating with your sumo instance
PORT = 8877
NB_PROCESSES = 6
# Parameters of the simulation
PENETRATION_RATIO = 0.1
SIM_TIME = 10000     # Simulation time (in sceonds)
WARMUP_TIME = 1000  # Simulation warm-up time (statistics are not taken into account)
STOP_PROB = 0
SCHEDULER_TYPE = 'RR' # enum('RR', 'BUF', 'MCF', 'MMF', 'MCF-RR', 'MMF-RR')
ARRIVAL_TYPE = 'infinite' # arrivalType in enum('fixed', 'Poisson', 'infinite')
SCHEDULERS = ['RR', 'BUF', 'MCF', 'MMF', 'MCF-RR', 'MMF-RR']
ARRIVALS = ['fixed', 'Poisson', 'infinite']
MAX_LOGICAL_PATHS = 10
LEAKAGE = 0.3
RATE = 0.25
ALPHA = 0.75 # EWMA parameter for the traveltime on the logical links
# alpha * x_t + (1-alpha) * x_t-1
RETRANSMISSION = 'local' # in enum('local', 'global')

ROAD_NETWORK = nx.DiGraph()