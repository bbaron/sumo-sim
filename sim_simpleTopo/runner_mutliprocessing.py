#!/usr/bin/env python
from __future__ import division
import os, sys
import optparse
import subprocess
import random
import itertools
from collections import deque
from operator import mul, itemgetter, attrgetter
from fractions import gcd
import math
import time
import re
import csv
import numpy as np
import networkx as nx
import cplex
from cplex.exceptions import CplexSolverError
from multiprocessing import Pool

from settings import *

# we need to import python modules from the $SUMO_HOME/tools directory
try:
    sys.path.append(os.path.join(os.environ.get("SUMO_HOME"), "tools")) # tutorial in docs
    sys.path.append(os.path.join(os.path.dirname('__file__'), '..'))
    from sumolib import checkBinary
except:
    sys.exit("please declare environment variable 'SUMO_HOME' as the root directory of your sumo installation (it should contain folders 'bin', 'tools' and 'docs')")

import traci
from classes import *
from tools import Vehicle, Data, Demand, ksp_yen, generateArrivals, get_closest_node_overlay, get_edges, select_edge, get_edge_attribute_from_name
from schedulers import *
from controller import *

def init(sch_type, arrival_type, leakage, rate):
    global ROAD_NETWORK
    """ Initilization function """

    print "### Overlay link characterization"
    net = nx.DiGraph()
    net.add_edge('S1', 'I1', name='e1', traffic=3, weight=1, travelTime=1000, leakage=leakage, flows=[])
    net.add_edge('S2', 'I1', name='e2', traffic=1, weight=1, travelTime=1000, leakage=leakage, flows=[])
    net.add_edge('I1', 'I2', name='e3', traffic=1, weight=1, travelTime=1000, leakage=leakage, flows=[])
    net.add_edge('I1', 'I3', name='e4', traffic=1, weight=1, travelTime=1000, leakage=leakage, flows=[])
    net.add_edge('I2', 'I3', name='e5', traffic=1, weight=1, travelTime=1000, leakage=leakage, flows=[])
    net.add_edge('I2', 'D1', name='e6', traffic=1, weight=1, travelTime=1000, leakage=leakage, flows=[])
    net.add_edge('I3', 'D2', name='e7', traffic=3, weight=1, travelTime=1000, leakage=leakage, flows=[])

    ROAD_NETWORK = net
    for node in ROAD_NETWORK.nodes():
        ROAD_NETWORK.node[node]['name'] = node

    demands = {
        'd1': Demand('d1', 'S1', 'D1', rate),
        'd2': Demand('d2', 'S1', 'D2', rate),
        'd3': Demand('d3', 'S2', 'D2', rate)
    } # Demand: (source, target, rate/size of data, demand paths {path name:path})

    print "### Initialize the demands"
    demand_weights = dict()
    if arrival_type == 'fixed':
        sum_demand_weights = sum([d.getRate() for d in demands.values()])
        demand_weights = dict([(demand, 86400*d.getRate()/sum_demand_weights) for (demand,d) in demands.items()])
    elif arrival_type == 'Poisson':
        demand_weights = dict([(demand, 86400*d.getRate()) for (demand,d) in demands.items()])

    sources = set([d.getOrigin() for d in demands.values()])
    targets = set([d.getTarget() for d in demands.values()])

    print "### Initialize the data at the offloading spots"
    if arrival_type in ['infinite', 'fixed']:
        for node in sources:
            net.node[node]['data'] = dict([(demand, sum([[Data(demands[demand], seq=demands[demand].incCounter())] for i in range(100)], [])) for demand,d in demands.items() if d.getOrigin() == node])

    print "### Initialize the paths at the edges"
    for (demand, d) in demands.items():
        # Compute the k-shortest paths between src (d[0]) and target (d[1])
        path_id = 1
        for ksp_data in ksp_yen(net, d.getOrigin(), d.getTarget(), max_k=MAX_LOGICAL_PATHS):
            (t,p) = ksp_data['cost'], ksp_data['path']

            path_name = demand+"p"+str(path_id) # dipj
            d.addPath(path_name, p)
            path_leakage = 1.0

            for (u,v) in get_edges(p):
                # As follows: (demand name, path name (for the demand), weight for the path)
                net[u][v]['flows'].append([demand, path_name, 1.0])
                net[u][v]['retransmissions'] = 1.0 / (1.0 - net[u][v]['leakage'])
                if 'data' not in net.node[u]:
                    net.node[u]['data'] = {}
                if 'expected_data' not in net.node[u]:
                    net.node[u]['expected_data'] = {}
                if 'transmitted_data' not in net.node[u]:
                    net.node[u]['transmitted_data'] = {}

                if 'data' not in net.node[v]:
                    net.node[v]['data'] = {}
                if 'expected_data' not in net.node[v]:
                    net.node[v]['expected_data'] = {}
                if 'transmitted_data' not in net.node[v]:
                    net.node[v]['transmitted_data'] = {}

                if demand not in net.node[u]['data']:
                    net.node[u]['data'][demand] = []
                if demand not in net.node[v]['data']:
                    net.node[v]['data'][demand] = []

                if not 'sch_data' in net.node[u]:
                    net.node[u]['sch_data'] = {}
                net.node[u]['sch_data'][net[u][v]['name']] = {}
                path_leakage *= (1.0 - net[u][v]['leakage'])

            path_id += 1
            retransmissions = 1.0
            if path_leakage < 1.0:
                retransmissions = 1.0 / (path_leakage)
            d.setPathRet(path_name, retransmissions)

            if __debug__:
                print demand, path_name, path_leakage, d.getPathRet(path_name)

    # Initialize the round robin
    for u in net.nodes():
        if 'sch_data' in net.node[u]:
            for edge in net.node[u]['sch_data'].keys():

                if sch_type in ('RR'):
                    demands_list = get_edge_attribute_from_name(net, edge, 'flows')
                    sequence = list(set([d[0] for d in demands_list]))
                    net.node[u]['sch_data'][edge] = {
                        'sch_seq': round_robin_generator(sequence),
                        'sch_seq_len': len(sequence)
                    }

                if sch_type in ('MCF-RR', 'MMF-RR'):
                    net.node[u]['sch_data'][edge] = {
                        'sch_seq': [],                 # Periodic sequence of demands
                        'sch_stack': deque(maxlen=10) # Stack for sequence indexes that have not been allocated
                    }

    return net, demands, demand_weights, sources, targets

def regenerate_data_at_source(net, demands, sources, src, arrival_type):
    if arrival_type == 'infinite' and src not in sources:
        return
    for demand in net.node[src]['data'].keys():
        if len(net.node[src]['data'][demand]) == 0:
            # Add more data to the source storage
            net.node[src]['data'][demand] += sum([[Data(demands[demand], seq=demands[demand].incCounter())] for i in range(100)], [])

def checkVehType(vehID):
    # Continue if the vehicle is not of type1 (the vehicles that are equipped with storage devices)
    if traci.vehicle.getTypeID(vehID) == "type1":
        return True
    else:
        return False

def sharesGPSInfo():
    return True # bool(random.getrandbits(1))

def dataInTransit(vehicles):
    """ Return the amount of data in transit (ie. carried by vehicles) """
    return len([veh for veh in vehicles.values() if veh.getData()])

def execScheduler(G,node,out_edge,sch_type):
    if sch_type == 'RR':
        return roundRobinScheduler(G,node,out_edge)
    elif sch_type == 'BUF':
        return bufferWeightedScheduler(G,node, out_edge)
    elif sch_type in ('MCF', 'MMF'):
        return weightedScheduler(G,node, out_edge)
    elif sch_type in ('MCF-RR', 'MMF-RR'):
        return weightedRoundRobinScheduler(G,node, out_edge)

def in_offloading_spot(node, vehID, step, vehicles, net, demands, sources, sch_type, arrival_type, ret):
    """ Vehicle vehID just arrived at offloading spot node at step """

    # If the arrival type is infinite, regenerate some data
    if arrival_type == 'infinite' and node in sources:
        regenerate_data_at_source(net, demands, sources, node, arrival_type)

    veh = vehicles[vehID]
    if veh.isSharingGPSInfo():
        # We have access to the vehicle's GPS information
        # Get the vehicle destination (next-hop logical node)
        out_edge = veh.getLogicalLink()
        other_node = veh.getTarget()
        # Pass the sequence to the scheduler to get the data to load
        demand = execScheduler(net, node, out_edge, sch_type)
        if not demand: # Do nothing
            return

        # Load the data on the vehicle
        if len(net.node[node]['data'][demand]) > 0:
            if __debug__:
                if node == "I1":
                    print "\t", node, len(net.node[node]['data'][demand]), net.node[node]['data'][demand][-1].startTime, net.node[node]['data'][demand][0].startTime

            data = net.node[node]['data'][demand].pop(0) # Remove the first item of the list (seen as a queue)
            if node in sources and not data.isRetransmitted:
                data.loadTime = step # First time the data is loaded on a vehicle
            else:
                data.endStop(node, step)

            veh.loadData(data)

            if (ret == 'global' and node in sources) or (ret == 'local'):
                # Add the data to the transmitted data queue of the node
                net.node[node]['transmitted_data'][data] = (step,other_node)

            # Notify the node at the other extremity of the logical link to expect the data
            travel_time = get_edge_attribute_from_name(net, out_edge, 'travelTime')
            timeout = step + 1.2*travel_time # Timeout for the retransmission of the data
            net.node[other_node]['expected_data'][data] = (timeout, node)

            if __debug__:
                print "[%s] edge %s - estimated travel time: %s" % (step, net[node][other_node]['name'], 1.2*travel_time)

            if __debug__:
                print "[%d] Vehicle %s arrived at node %s takes %s" % (step, vehID, node, data)
    else:
        pass


def out_offloading_spot(node, vehID, step, vehicles, net, demands, targets, ret):
    """ Vehicle vehID just left at offloading spot node at step """

    veh = vehicles[vehID]

    # data drop-off
    data = veh.unloadData()
    if data and data in net.node[node]['expected_data']:
        if node in targets and (node == data.getDemand().getTarget()):
            if ret == 'global':
                # Delete copy of the data in the source buffer
                src = data.getDemand().getOrigin()
                del net.node[src]['transmitted_data'][data]
            data.endTime = step
        else:
            # Check whether the data has arrived at the right destination
            if data.getDemand().name not in net.node[node]['data'].keys():
                print "Forwarding error data %s received at node %s" % (data, node)
                return

            data.startStop(node, step)

        # Add the data to the buffer of transhipped data
        net.node[node]['data'][data.getDemand().name].append(data)

        other_node = veh.getOrigin()
        # Compute the EWMA of the travel time of the logical link
        old_travelTime = net[other_node][node]['travelTime']
        new_travelTime = step - data.getStop(other_node, 'pickup')
        net[other_node][node]['travelTime'] = ALPHA * new_travelTime + (1-ALPHA) * old_travelTime

        if __debug__:
            print "[%s] edge %s - actual travel time: %s" % (step, net[other_node][node]['name'], net[other_node][node]['travelTime'])

        # Notify the other node (at the other extremity of the logical link) the data was recieved
        # => Delete the data in the queue
        if ret == 'local':
            del net.node[other_node]['transmitted_data'][data]

        # Delete the data that the node was expecting
        del net.node[node]['expected_data'][data]

        if __debug__:
            print "[%d] Vehicle %s left at node %s brought %s" % (step, vehID, node, data)

def run_sumo_sim(output_file, port=PORT):
    """ Run the simulation without doing anything """

    traci.init(port)
    f = open(output_file,'w')

    step = 0
    print_count = 0
    vehicles = {}
    while step < SIM_TIME:
        if print_count == 1000:
            print "[%s]" % step
            print_count = 0

        traci.simulationStep()

        enteredSim = traci.simulation.getDepartedIDList()
        for vehID in enteredSim:
            if not checkVehType(vehID): continue

            # Get the node where the vehicle appeared
            vehRoute = traci.vehicle.getRoute(vehID)
            vehOrigin = ROAD_NETWORK.node[select_edge(ROAD_NETWORK, 'name', vehRoute[0])[0]]['name']
            vehTarget = ROAD_NETWORK.node[select_edge(ROAD_NETWORK, 'name', vehRoute[-1])[1]]['name']
            vehLogicalLink = net[vehOrigin][vehTarget]['name']

            # Add the vehicle in the vehicles dictionary
            vehicles[vehID] = (step,vehOrigin,vehTarget,vehLogicalLink)

            # Create a new event in the file
            f.write("%s;%d;%s;%s;%s;%s\n" % ('IN', step, vehID, vehOrigin, vehTarget, vehLogicalLink))

        exitedSim = traci.simulation.getArrivedIDList()
        for vehID in exitedSim:
            if vehID not in vehicles: continue

            # Retreive the informations related to the vehicle
            (start_step,vehOrigin,vehTarget,vehLogicalLink) = vehicles[vehID]

            f.write("%s;%d;%s\n" % ('OUT', step, vehID))

            # Delete the vehicle entry
            del vehicles[vehID]

        # Increment the timestep and print counter
        step += 1
        print_count += 1

    traci.close()
    f.close()
    sys.stdout.flush()

def run_simulation(input_file, net, demands, sources, targets, leakage, sch_type, arrival_type, ret, output_file_storage=False, stop_sim=-1):
    """ Run the simulation based on the input file
        The input file is the same format as the one generated with @run_sumo_sim() """

    vehicles = dict()

    if arrival_type == 'Poisson':
        arrivals = generateArrivals(demands, SIM_TIME)

    if output_file_storage:
        output_file = 'results/I1_storage_%s_%s.txt' % (sch_type, arrival_type)
        f_storage = open(output_file, 'w')
        f_storage.write("step;%s\n" % (';'.join([d for d in net.node['I1']['data'].keys()])))

    storage = AutoVivification()
    # Initialize the storage array
    storage['transmitted_data'] = 0
    for d in net.node['I1']['data'].keys():
        storage['data'][d] = 0


    print storage

    current_timestep = 0
    with open(input_file, 'r') as f:
        reader = csv.reader(f,delimiter=';')
        for row in reader:
            event = row[0] # 'IN' or 'OUT'
            step  = int(row[1])

            if step != current_timestep:
                # The current timestep has changed
                current_timestep = step
                if stop_sim != -1 and current_timestep >= stop_sim:
                    break

                if __debug__:
                    print "\t".join(["%s [%s]" % (n, " ".join(["%s %s" % (d, len(net.node[n]['data'][d])) for d in net.node[n]['data'].keys()])) for n in net.nodes()])

                if((current_timestep % 1000) == 0):
                    print "[%s / %s / %s / %s / %g]" % (current_timestep, sch_type, arrival_type, ret, leakage)

                # Check the expired data for all the nodes
                for node in net.nodes():
                    if not 'expected_data' in net.node[node]: continue
                    for (data,(expected_time,origin_node)) in net.node[node]['expected_data'].items():
                        if expected_time < current_timestep:
                            # Data is expired => retransmit the data at the origin node
                            if ret == 'local':
                                net.node[origin_node]['data'][data.getDemand().name].insert(0,data)
                            elif ret == 'global':
                                # Retransmit the data at the source of the demand
                                data.isRetransmitted = True
                                src = demands[data.getDemand().name].getOrigin()
                                net.node[src]['data'][data.getDemand().name].insert(0,data)

                            if __debug__:
                                print "[%s] Data %s is missing at node %s from node %s" % (step, data.getDemand().name, node, origin_node)

                            # In any case, delete the expected data at the node
                            del net.node[node]['expected_data'][data]


                if output_file_storage:
                    # Write the state of the storage at node I1
                    f_storage.write("%s;%s\n" % (step,';'.join([str(len(net.node['I1']['data'][d])) for d in net.node['I1']['data'].keys()])))

                # Compute the storage levels for the node I1
                storage['transmitted_data'] += len(net.node['I1']['transmitted_data'])
                for d in net.node['I1']['data'].keys():
                    storage['data'][d] += len(net.node['I1']['data'][d])

                if arrival_type == 'Poisson' :
                    # Generate the data in the corresponding buffer according to a Poisson process
                    while (len(arrivals) > 0) and (math.floor(arrivals[0][1]) <= current_timestep):
                        d = arrivals.pop(0)
                        src = demands[d[0]].getOrigin()
                        net.node[src]['data'][d[0]].append(Data(demands[d[0]], startTime=current_timestep, seq=demands[d[0]].incCounter()))

            vehID = row[2]

            if event == 'IN': # The vehicle entered in the simulation
                vehOrigin      = row[3]
                vehTarget      = row[4]
                vehLogicalLink = row[5]

                if not vehID in vehicles:
                    vehicles[vehID] = Vehicle(vehID, vehOrigin, vehTarget, vehLogicalLink)

                # Load the data at the offloading spot
                in_offloading_spot(vehOrigin, vehID, step, vehicles, net, demands, sources, sch_type, arrival_type, ret)
            elif event == 'OUT' and random.random() >= leakage:
                # The vehicle left the simulation and is not leaking
                if vehID not in vehicles: continue

                veh = vehicles[vehID]
                if not veh.hasData(): continue

                # Unload the data at the offloading spot
                out_offloading_spot(veh.getTarget(), vehID, step, vehicles, net, demands, targets, ret)
                # Delete the vehicle from the vehicles dictionary
                del vehicles[vehID]

            if __debug__:
                for demand in demands.keys():
                    print "%s: %s" % (demand, " ".join("%s %s " % tup for tup in [(node, len(net.node[node]['data'][demand])) for node in net.nodes() if demand in net.node[node]['data']]))

            if arrival_type == 'fixed':
                if sum([d.getRate() == len(net.node[d.getTarget()]['data'][demand]) for (demand, d) in demands.items()]) == len(demands):
                    break

    # Compute statistics on the data arrived at destination
    res = {}
    aggregated_throughput = 0.0
    aggregated_opt = 0.0
    store_tran = storage['transmitted_data'] / step
    aggregated_data = 0.0

    for demand in demands.keys():
        target = demands[demand].getTarget()
        rate   = demands[demand].getRate()
        store_data = storage['data'][demand] / step
        optimal = sum([p['flow'] for p in demands[demand].getPaths().values() if p['flow']])

        if not rate: rate = -1
        sumTimes = 0
        dataLength = 0
        for data in net.node[target]['data'][demand]:
            if data.loadTime < WARMUP_TIME:
                continue
            if arrival_type == "Poisson":
                sumTimes += (data.endTime - data.startTime)
            else:
                sumTimes += (data.endTime - data.loadTime)
            dataLength += 1

        throughput = dataLength / (step)
        aggregated_throughput += throughput
        aggregated_opt += optimal
        aggregated_data += store_data

        if dataLength > 0:
            res[demand] = (sch_type, arrival_type, leakage, ret, rate, dataLength, sumTimes/dataLength, throughput, optimal, store_data)
            print "%s;%s;%s;%s;%g;%g;%g;%g;%g;%g;%g" % (sch_type, arrival_type, demand, ret, rate, leakage, dataLength, sumTimes/dataLength, throughput, optimal, store_data)
        else:
            res[demand] = (sch_type, arrival_type, leakage, ret, rate, dataLength, 0, throughput, optimal, store_data)
            print "%s;%s;%s;%s;%g;%g;%g;%g;%g;%g;%g" % (sch_type, arrival_type, demand, ret, rate, leakage, dataLength, 0, throughput, optimal, store_data)

    res['all'] = (sch_type, arrival_type, leakage, ret, rate, 0, 0, aggregated_throughput, aggregated_opt, store_tran)
    print "%s;%s;%s;%s;%g;%g;%g;%g;%g;%g;%g" % (sch_type, arrival_type, 'all', ret, rate, leakage, 0, 0, aggregated_throughput, aggregated_opt, store_tran)

    if output_file_storage:
        f_storage.close()

    return res


def run(vehicles, net, demands, sources, targets, port=PORT):
    """ execute the TraCI control loop
        - port is the port used for this run of the simulation """

    traci.init(port)

    if arrival_type == 'Poisson':
        arrivals = generateArrivals(demands, SIM_TIME)

    to_print = 0
    step = 0
    while step < SIM_TIME:
        # traci.simulation.getMinExpectedNumber()
        traci.simulationStep()

        if arrival_type == 'Poisson' :
            # Generate the data in the corresponding buffer according to a Poisson process
            while (len(arrivals) > 0) and (math.floor(arrivals[0][1]) == step):
                d = arrivals.pop(0)
                src = demands[d[0]].getOrigin()
                net.node[src]['data'][d[0]].append(Data(d[0], startTime = step))

        # Get the vehicles ids that entered the simulation ("departed")
        enteredSim = traci.simulation.getDepartedIDList()
        for vehID in enteredSim:
            if not checkVehType(vehID):
                continue

            # Add the vehicle to the vehicles dictionary
            vehRoute = traci.vehicle.getRoute(vehID)
            vehOrigin = ROAD_NETWORK.node[select_edge(ROAD_NETWORK, 'name', vehRoute[0])[0]]['name']
            vehTarget = ROAD_NETWORK.node[select_edge(ROAD_NETWORK, 'name', vehRoute[-1])[1]]['name']
            vehLogicalLink = net[vehOrigin][vehTarget]['name']

            if not vehID in vehicles:
                vehicles[vehID] = Vehicle(vehID, vehOrigin, vehTarget, vehLogicalLink)

            # Load the data at the offloading spot
            in_offloading_spot(vehOrigin, vehID, step)

        # Get the vehicles ids that exited the simulation ("arrived")
        exitedSim = traci.simulation.getArrivedIDList()
        for vehID in exitedSim:
            if vehID not in vehicles:
                continue

            veh = vehicles[vehID]
            if not veh.hasData():
                continue

            # Unload the data at the offloading spot
            out_offloading_spot(veh.getTarget(), vehID, step)
            # Delete the vehicle from the vehicles dictionary
            del vehicles[vehID]

        # Print STATS
        if to_print == 100:
            for demand in demands.keys():
                print "%s: %s" % (demand, " ".join("%s %s " % tup for tup in [(node, len(net.node[node]['data'][demand])) for node in net.nodes() if 'data' in net.node[node] and demand in net.node[node]['data']]))
                to_print = 0
            # for node, demand in [(node, [(demand, len(data)) for demand,data in d['data'].items()]) for node, d in net.nodes(data=True)]:
            #     print "%s: %s" % (node, " ".join("(%s,%s)" % tup for tup in demand))

        step += 1
        to_print += 1

        if arrival_type == 'fixed':
            if sum([demands[demand].getRate() == len(net.node[d[1]]['data'][demand]) for (demand, d) in demands.items()]) == len(demands):
                break

    # Compute statistics on the data arrived at destination
    print step, SIM_TIME
    for target in targets:
        for demand in net.node[target]['data'].keys():
            sumTimes = 0
            dataLength = len(net.node[target]['data'][demand])
            for data in net.node[target]['data'][demand]:
                sumTimes += (data.endTime - data.loadTime)
            if dataLength > 0:
                print "%s %s %g %g %g" % (target, demand, dataLength, sumTimes/dataLength, dataLength / (step))
            else:
                print "%s %s %g %g %g" % (target, demand, dataLength, 0, dataLength / (step))

    traci.close()
    sys.stdout.flush()

def print_net_stats(net):
    for n in net.nodes():
        print '## %s: [ %s]' % (n,' '.join("%s %s " % tup for tup in [(d,len(data)) for d,data in net.node[n]['data'].items()]))
        if 'sch_data' in net.node[n]:
            for e in net.node[n]['sch_data'].keys():
                print '\t', e, net.node[n]['sch_data'][e]
    print
    for u,v in net.edges():
        print '## %s (traffic:%s, weight:%s)' % (net[u][v]['name'], net[u][v]['traffic'], net[u][v]['weight'])
        for p in net[u][v]['flows']:
            print '\t', p

def get_options():
    optParser = optparse.OptionParser()
    optParser.add_option("-s",  action="store", dest='scheduler', help="define the scheduler")
    optParser.add_option("-a",  action="store", dest='arrival', help='define the data arrival type')
    optParser.add_option("-g",  action="store", dest='to_event_file', help='Generate event file')
    optParser.add_option("-r",  action="store", dest='from_event_file', help='Read event file and run the simulation')
    optParser.add_option("-p",  action="store", dest='port', help='Set simulation port')
    optParser.add_option("-l",  action="store", dest='stop_sim', help='Set the length of the simulation (stop time)', default=-1, type="int")
    optParser.add_option("-m",  action="store_true", dest='mulproc', help='Run the simulation with multiprocessing', default=False)

    options, args = optParser.parse_args()
    return options

def run_sumo(port):
    sumoBinary = checkBinary('sumo')
    sumoProcess = subprocess.Popen([sumoBinary, "-c", "sim.sumo.cfg", "--remote-port", str(port)], stdout=sys.stdout, stderr=sys.stderr)
    traci.init(port)
    print "process running", port
    # Get output of run
    traci.close()
    sumoProcess.wait()

def run_sim((sch_type, arrival_type, arg, ret)):
    if arrival_type == 'Poisson':
        rate = arg
        leakage = LEAKAGE
        print "[%s / %s / %s / %g / %g]" % (sch_type, arrival_type, ret, leakage, rate)

    elif arrival_type == 'infinite':
        leakage = arg
        rate = None
        print "[%s / %s / %s / %g]" % (sch_type, arrival_type, ret, leakage)

    else:
        return

    net, demands, demand_weights, sources, targets = init(sch_type, arrival_type, leakage, rate)
    res = None
    if sch_type in ['MCF', 'MCF-RR']:
        res = multiCommodityFlow(net, demand_weights, arrival_type, demands, sch_type, ret)
    if sch_type in ['MMF', 'MMF-RR']:
        res = maxMinFairness(net, demand_weights, arrival_type, demands, sch_type, ret)

    return run_simulation(IN_FILE, net, demands, sources, targets, leakage, sch_type, arrival_type, ret)

# this is the main entry point of this script
if __name__ == "__main__":
    sumoBinary = checkBinary('sumo')
    options = get_options()

    IN_FILE = options.from_event_file

    arrival_type = ARRIVAL_TYPE
    if options.arrival in ARRIVALS:
        arrival_type = options.arrival

    if options.mulproc:
        pool = Pool(processes=NB_PROCESSES)
        if arrival_type == 'Poisson':
            # Compute the average end-to-end delay of the data transfers as a function of the demand rates
            # Wth fixed leakage of 0.3
            args = [(sch, arrival_type, r, ret) for sch in ['RR','MCF', 'MMF'] for r in np.arange(0.01,1.05,0.05) for ret in ['global', 'local']]

        elif arrival_type == 'infinite':
            # Compute the average throughput of the data transfers as a function of the leakage
            args = [(sch, arrival_type, l, ret) for sch in ['RR','MCF','MMF'] for l in np.arange(0.01,0.5,0.05) for ret in ['global', 'local']]
        print args

        res = pool.map(run_sim, args, 1)
        with open("results-sim.txt", 'w') as f:
            for el in res:
                for d, data in el.items():
                    f.write("%s;%s\n" % (d,";".join(str(e) for e in data)))
        print res
        sys.exit()

    vehicles = dict()
    sch_type = SCHEDULER_TYPE
    ret = RETRANSMISSION
    leakage = LEAKAGE
    rate = RATE

    if options.scheduler in SCHEDULERS:
        sch_type = options.scheduler
    if options.port:
        PORT = options.port

    print "%s / %s" % (sch_type, arrival_type)

    net, demands, demand_weights, sources, targets = init(sch_type, arrival_type, leakage, rate)
    res = None
    if sch_type in ['MCF', 'MCF-RR']:
        multiCommodityFlow(net, demand_weights, arrival_type, demands, sch_type, ret)
    if sch_type in ['MMF', 'MMF-RR']:
        maxMinFairness(net, demand_weights, arrival_type, demands, sch_type, ret)

    if options.to_event_file:
        sumoProcess = subprocess.Popen([sumoBinary, "-c", "sim.sumo.cfg", "--remote-port", str(PORT)], stdout=sys.stdout, stderr=sys.stderr)
        run_sumo_sim(options.to_event_file)
        sumoProcess.wait()
    if options.from_event_file:
        run_simulation(IN_FILE, net, demands, sources, targets, leakage, sch_type, arrival_type, ret, stop_sim=options.stop_sim)
