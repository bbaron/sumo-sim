import random
import time 

from tools import *

def weightedScheduler(G, node, out_edge):
    if 'sch_data' not in G.node[node] or out_edge not in G.node[node]['sch_data']:
        return None

    g = G.node[node]['sch_data'][out_edge]['sch_seq']
    seq_len = G.node[node]['sch_data'][out_edge]['sch_seq_len']

    if seq_len > 0:
        val = g.next()

        return val
    else:
        return None

def weightedScheduler_old(G, node, out_edge):
    demands_list = get_edge_attribute_from_name(G, out_edge, 'flows')
    demands = [(demand[0], demand[2]) for demand in demands_list if demand[2] > 0.0]
    capacity = get_edge_attribute_from_name(G, out_edge, 'traffic')

    # Compute the weights for the outgoing edge
    weights = []
    rem_capacity = capacity
    for (demand, d) in demands:
        weights.append((demand, int(100*d/capacity)))
        rem_capacity -= d
    weights.append((None, int(100*rem_capacity/capacity)))

    targetList = sum([[demand]*w for (demand, w) in weights], [])
    if len(targetList) > 0:
        return random.choice(targetList)
    return None

def weightedRoundRobinScheduler(G, node, out_edge):
    # Get the current round robin scheduler index
    if 'sch_data' not in G.node[node] or out_edge not in G.node[node]['sch_data']:
        return None

    g = G.node[node]['sch_data'][out_edge]['sch_seq']
    seq_len = G.node[node]['sch_data'][out_edge]['sch_seq_len']
    if seq_len == 0: return None
    
    stack = G.node[node]['sch_data'][out_edge]['sch_stack']
    if __debug__:
        print "\t", node, out_edge, i, seq, stack, [(d, len(data)) for d,data in G.node[node]['data'].items()]

    # empty the stack
    j = 0
    while j < len(stack):
        d = G.node[node]['sch_data'][out_edge]['sch_stack'].pop()
        if len(G.node[node]['data'][d]) > 0:
            return d
        j += 1

    # Choose the demand to schedule according to the scehuling sequence
    j = 0
    tmp_stack = []
    val = g.next()
    while j < seq_len and len(G.node[node]['data'][val]) == 0:
        # Append the missed element to the stack
        tmp_stack.append(val)
        val = g.next()
        j += 1

    if j >= seq_len: return None

    # Append the tmp stack to the actual stack
    G.node[node]['sch_data'][out_edge]['sch_stack'].extend(tmp_stack)
    return val

def roundRobinScheduler(G, node, out_edge):
    # Get the current round robin scheduler index
    if 'sch_data' not in G.node[node] or out_edge not in G.node[node]['sch_data']:
        return None

    g = G.node[node]['sch_data'][out_edge]['sch_seq']
    seq_len = G.node[node]['sch_data'][out_edge]['sch_seq_len']
    
    j = 0
    val = g.next()
    while j < seq_len and len(G.node[node]['data'][val]) == 0:
        val = g.next()
        j += 1

    if j >= seq_len:
        return None
    else:
        return val


def bufferWeightedScheduler(G, node, out_edge):
    demands_list = get_edge_attribute_from_name(G, out_edge, 'flows')
    demands = [demand[0] for demand in demands_list]
    data = dict([(demand, len(G.node[node]['data'][demand])) for demand in demands])
    sumData = sum(data.values())
    if sumData == 0: return None
    max_value = max(data.itervalues())
    max_demands = [k for k in data if data[k] == max_value]
    if len(max_demands) > 1:
        return random.choice(max_demands)
    else:
        return max_demands[0]
