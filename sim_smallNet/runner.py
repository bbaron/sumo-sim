#!/usr/bin/env python
from __future__ import division
import os, sys,getopt
import optparse
import subprocess
import random
import itertools
from operator import mul, itemgetter, attrgetter
from collections import Counter, deque
from fractions import gcd
import math
import time
import re
import numpy as np
import networkx as nx
from decimal import *
from fractions import gcd
import cplex
from cplex.exceptions import CplexSolverError

# we need to import python modules from the $SUMO_HOME/tools directory
try:
    sys.path.append(os.path.join(os.path.dirname('__file__'), '..', '..', '..', '..', "tools")) # tutorial in tests
    sys.path.append(os.path.join(os.environ.get("SUMO_HOME", os.path.join(os.path.dirname('__file__'), "..", "..", "..")), "tools")) # tutorial in docs
    from sumolib import checkBinary
except ImportError:
    sys.exit("please declare environment variable 'SUMO_HOME' as the root directory of your sumo installation (it should contain folders 'bin', 'tools' and 'docs')")

import traci
# the port used for communicating with your sumo instance
PORT = 8876

class AutoVivification(dict):
    """Implementation of perl's autovivification feature."""
    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value

class Data(object):
    """ This class represents a chunk of data that a vehicle can carry """
    def __init__(self, demand, startTime=0):
        self.demand = demand
        self.startTime = startTime
        self.loadTime = startTime
        self.stops = {} # Format: {node: [start_stop, end_stop]}
        self.endTime = -1

    def getDemand(self):
        return self.demand

    def startStop(self, node, step):
        self.stops[node] = [step]

    def endStop(self, node, step):
        self.stops[node].append(step)

    def __str__(self):
        return "# Data " + self.demand + " #"

class Vehicle(object):
    """ This class represents a vehicle that is equipped with data storage device (of type1) """
    def __init__(self, vehID, vehRoute):
        self.vehID = vehID
        self.vehRoute = vehRoute
        self.data = None
        self.sharesGPSInfo = sharesGPSInfo()

    def hasData(self):
        return self.data

    def loadData(self, data):
        self.data = data

    def unloadData(self):
        data = self.data
        self.data = None
        return data

    def getRoute(self):
        return self.vehRoute

    def getAllStops(self):
        return traci.vehicle.getStops(self.vehID)

    def getData(self):
        return self.data

    def getDestination(self):
        return self.vehRoute[-1]

    def getSource(self):
        return self.vehRoute[1]

    def isSharingGPSInfo(self):
        return self.sharesGPSInfo

# Parameters of the simulation
PENETRATION_RATIO = 0.1
SIM_TIME = 5000     # Simulation time (in sceonds)
WARMUP_TIME = 1000  # Simulation warm-up time (statistics are not taken into account)
STOP_PROB = 0
SCHEDULER_TYPE = 'RR' # enum('RR', 'BUF', 'MCF', 'MMF', 'MCF-RR', 'MMF-RR')
ARRIVAL_TYPE = 'Poisson' # arrivalType in enum('fixed', 'Poisson', 'infinite')
SCHEDULERS = ['RR', 'BUF', 'MCF', 'MMF', 'MCF-RR', 'MMF-RR']
ARRIVALS = ['fixed', 'Poisson', 'infinite']

output_file = 'results/res.txt'

myVehicles = dict() # Global dictionary that stores all of the vehicles equipped with data storage devices
DEMANDS = { 
    'd1': ['S1', 'D1', 0.4, {}],
    'd2': ['S1', 'D2', 0.4, {}],
    'd3': ['S2', 'D2', 0.4, {}]
} # Demand: (source, target, rate/size of data, demand paths {path name:path})

if ARRIVAL_TYPE == 'fixed':
    sum_demand_weights = sum([d[2] for d in DEMANDS.values()])
    DEMAND_WEIGHTS = dict([(demand, d[2]/sum_demand_weights) for (demand,d) in DEMANDS.items()])
elif ARRIVAL_TYPE == 'Poisson':
    DEMAND_WEIGHTS = dict([(demand, d[2]) for (demand,d) in DEMANDS.items()])

SOURCES = set([d[0] for d in DEMANDS.values()])
TARGETS = set([d[1] for d in DEMANDS.values()])

NET = nx.DiGraph()
NET.add_edge('S1', 'I1', name='e1', traffic=1, weight=1, paths=[])
NET.add_edge('S2', 'I1', name='e2', traffic=1, weight=1, paths=[])
NET.add_edge('I1', 'I2', name='e3', traffic=1, weight=1, paths=[])
NET.add_edge('I1', 'I3', name='e4', traffic=1, weight=1, paths=[])
NET.add_edge('I2', 'I3', name='e5', traffic=1, weight=1, paths=[])
NET.add_edge('I2', 'D1', name='e6', traffic=1, weight=1, paths=[])
NET.add_edge('I3', 'D2', name='e7', traffic=1, weight=1, paths=[])

def init():
    global NET, DEMANDS
    """ Initilization function """

    # Initialize the data at the offloading spots
    if ARRIVAL_TYPE in ['infinite', 'fixed']:
        for node in SOURCES:
            NET.node[node]['data'] = dict([(demand, [Data(demand)]*DEMANDS[demand][2]) for demand in get_demands_from_source(node)])

    # Initialize the paths at the edges
    for (demand, d) in DEMANDS.items():
        # Compute the k-shortest paths between src (d[0]) and target (d[1])
        path_id = 1
        for ksp_data in ksp_yen(NET, d[0], d[1], max_k=10):
            (t,p) = ksp_data['cost'], ksp_data['path']
            path_name = demand+"p"+str(path_id)
            d[3][path_name] = p
            for (u,v) in get_edges(p):
                # As follows: (demand name, path name (for the demand), weight for the path)
                NET[u][v]['paths'].append([demand, path_name, 1.0])
                if 'data' not in NET.node[u]:
                    NET.node[u]['data'] = {}
                if 'data' not in NET.node[v]:
                    NET.node[v]['data'] = {}
                if demand not in NET.node[u]['data']:
                    NET.node[u]['data'][demand] = []
                if demand not in NET.node[v]['data']:
                    NET.node[v]['data'][demand] = []

                if not 'sch_data' in NET.node[u]:
                    NET.node[u]['sch_data'] = {}
                NET.node[u]['sch_data'][NET[u][v]['name']] = {'cur': 0} # if round robin scheduler (index increment)
            path_id += 1

    # Initialize the round robin
    if SCHEDULER_TYPE in ('MCF-RR', 'MMF-RR'):
        for u in NET.nodes():
            if 'sch_data' in NET.node[u]:
                for edge in NET.node[u]['sch_data'].keys():
                    NET.node[u]['sch_data'][edge] = {
                        'sch_seq': [],                 # Periodic sequence of demands
                        'sch_stack': deque(maxlen=10), # Stack for sequence indexes that have not been allocated
                        'cur': 0                       # Current index of the sequence
                    }


def regenerate_data_at_source(src):
    global NET

    if ARRIVAL_TYPE == 'infinite' and src not in SOURCES:
        return
    for demand in NET.node[src]['data'].keys():
        if len(NET.node[src]['data'][demand]) == 0:
            # Add more data to the source storage
            NET.node[src]['data'][demand] += [Data(demand)]*DEMANDS[demand][2]

def feq(a,b):
    return abs(a - b) <= 0.0001

def round_float(x,n=2):
    return float('%.*f' % (n,round(x, n)))

def apply_weights(G, paths_weights, full=True):
    """ Function that applies the weights decided in the flow allocation in the graph G """

    # Apply the weights for the logical links
    for u,v in NET.edges():
        for p in NET[u][v]['paths']:
            if p[1] in paths_weights:
                p[2] = paths_weights[p[1]]
            else:
                p[2] = 0.0

    if SCHEDULER_TYPE in ['MCF-RR', 'MMF-RR']:
        for u in NET.nodes():
            if 'sch_data' in NET.node[u]:
                for edge in NET.node[u]['sch_data']:
                    e_demands = get_edge_attribute_from_name(NET, edge, 'paths')
                    d_weights = {}
                    for d in e_demands:
                        if d[0] not in d_weights:
                            d_weights[d[0]] = 0.0
                        d_weights[d[0]] += d[2]
                    # Fill the rest of the capacity with None
                    if not full:
                        d_weights[None] = 1.0 - sum(w for w in d_weights.values())

                    if __debug__:
                        print u, edge, d_weights

                    NET.node[u]['sch_data'][edge]['sch_seq'] = round_robin_seq(d_weights)

def interleave_seq(lists):
    return [item[1] for item in 
        sorted(itertools.chain(*(
            zip(itertools.count(1.0/len(seq), 1.0/len(seq)), seq)
            for seq in lists if seq)))] 

def round_robin_seq(d_weights):
    """ Returns the round robin sequence from the demand_weights 
         - d_weights: {'di': w_i}
    """
    # Create the sequence for the weights
    weights = dict(map(lambda (d,x): (d,int(round_float(x)*100)), d_weights.items()))
    div = reduce(gcd,weights.values())
    
    if div <= 0: return []
    
    lists = [[demand]*int(w/div) for (demand, w) in weights.items()]

    return interleave_seq(lists)

def get_edges(path):
    """ Returns the list of edges from a list of nodes (path)
    """
    return zip(path[0:], path[1:])

def select_edge(G, attr_name, attr_value):
    """ Returns the edge that features the given attribute (name,value) """
    return [(u,v) for u,v,d in G.edges_iter(data=True) if d[attr_name] == attr_value][0]

def get_edge_attribute_from_name(G, edge, attr_name):
    """ Returns the attribute name of edge from its name """
    (u,v) = select_edge(G, 'name', edge)
    return G[u][v][attr_name]

def get_edges_in(G, node):
    """ Returns the list of the names of incoming edges at node """
    return [d['name'] for u,v,d in G.in_edges_iter(node, data=True)]

def get_edges_out(G, node):
    """ Returns the list of the names of outgoing edges at node """
    return [d['name'] for u,v,d in G.out_edges_iter(node, data=True)]

def get_demand_targets_from_source(src):
    return [d[1] for d in DEMANDS.values() if d[0] == src]

def get_demands_from_source(src):
    return [demand for demand, d in DEMANDS.items() if d[0] == src]

def get_node_from_edge(G, edge, where):
    """ Returns the node from the edge name and whether the vehicle is departing or arriving at the edge
         - where in enum('in', 'out') """
    (u,v) = select_edge(G, 'name', edge)
    if where == 'in': 
        return u
    elif where == 'out': 
        return v
    return None

def arrivalGenerator(rate):
    t = 0
    while True:
        t += random.expovariate(rate)
        yield t

def generateArrivals():
    """ Generate the list of Poisson arrivals for the two data streams """
    t = [(i, 0) for i in DEMANDS.keys()]
    arrivals = []
    for i in DEMANDS.keys():
        t = 0
        if DEMANDS[i][2] > 0:
            generator = arrivalGenerator(DEMANDS[i][2])
            arrivals += [(i, arrival) \
                         for arrival in itertools.takewhile(lambda t: t < SIM_TIME, generator)]

    sorted_arrivals = sorted(arrivals, key=lambda x: x[1])

    return sorted_arrivals

def myRound(x):
    if x >= 1: return round(x)
    return x

def is_target(node):
    return node in TARGETS

def get_edges_from_nodes(path):
    return [NET[i][j]['name'] for (i,j) in zip(path[0:], path[1:])]

def ksp_yen(G, node_start, node_end, max_k=2, weight='weight'):
    """ Return the  k-shortest paths between node_start and node_end, following the given weight.
        The function follows Yen's algorithm.
        Returns a dictionary indexed with keys 'cost' and 'path'. 
    """
    graph = G.copy()    # Deep copy of the graph to work on it
    distances, path = nx.single_source_dijkstra(graph, node_start, weight=weight)

    A = [{'cost': distances[node_end], 
          'path': path[node_end]}]
    B = []

    if not A[0]['path']: return A

    for k in range(1, int(max_k)):
        for i in range(0, len(A[-1]['path']) - 1):
            node_spur = A[-1]['path'][i]
            path_root = A[-1]['path'][:i+1]

            for path_k in A:
                curr_path = path_k['path']
                if len(curr_path) > i and path_root == curr_path[:i+1] and graph.has_edge(curr_path[i], curr_path[i+1]):
                    graph.remove_edge(curr_path[i], curr_path[i+1])
            
            distances_spur, path_spur = nx.single_source_dijkstra(graph, node_spur, node_end, weight=weight)

            if node_end in path_spur and path_spur[node_end]:
                path_total = path_root[:-1] + path_spur[node_end]
                dist_total = distances[node_spur] + distances_spur[node_end]
                potential_k = {'cost': dist_total, 'path': path_total}

                if not (potential_k in B):
                    B.append(potential_k)
        if len(B):
            B = sorted(B, key=itemgetter('cost'))
            A.append(B[0])
            B.pop(0)
        else:
            break

    return A


def get_edges_to_dest(src, dest):
    return get_edges_from_nodes(nx.dijkstra_path(NET, src, dest))

def generate_routefile():
    """ This function generates the route file from the attributes given in the command line """

    with open("sim.rou.xml", "w") as routes:
        print >> routes, """<routes>
    <!-- Types of vehicles -->
    <vTypeDistribution id="typedist1">"""
        print >> routes, '        <vType id="type1" accel="0.8" vClass="passenger"  length="5" maxSpeed="70" probability="%.2f" color="0,0,1" />' % (1.0-PENETRATION_RATIO)
        print >> routes, '        <vType id="type2" accel="0.8" vClass="passenger"  length="5" maxSpeed="70" probability="%.2f" color="0,1,1" />' % (PENETRATION_RATIO)
        print >> routes, "    </vTypeDistribution>"

        print >> routes, '    <!-- Flows -->'
        flow_sum = 0
        for (u,v) in NET.edges():
            traffic = NET[u][v]['traffic']
            if traffic > 0:
                print >> routes, '    <flow id="flow%d" departPos="0" departLane="random" departSpeed="max" type="typedist1" begin="0.00" vehsPerHour="%.2f">' % (flow_sum, traffic*3600)
                print >> routes, '        <route edges="%s"/>' % (NET[u][v]['name'])
                print >> routes, '    </flow>'
            flow_sum += 1
        print >> routes, '</routes>'

def weightedScheduler_full(node, out_edge):
    demands_list = get_edge_attribute_from_name(NET, out_edge, 'paths')
    demands = [(demand[0], demand[2]) for demand in demands_list if demand[2] > 0]

    # Compute the weights for the outgoing edge
    weights = dict(map(lambda (d,x): (d,int(round_float(x)*100)), demands))
    targetList = sum([[demand]*w for (demand, w) in weights.items()], [])

    if len(targetList) > 0:
        return random.choice(targetList)
    return None

def weightedScheduler(node, out_edge):
    demands_list = get_edge_attribute_from_name(NET, out_edge, 'paths')
    demands = [(demand[0], demand[2]) for demand in demands_list if demand[2] > 0]
    capacity = get_edge_attribute_from_name(NET, out_edge, 'traffic')

    # Compute the weights for the outgoing edge
    weights = []
    rem_capacity = capacity
    for (demand, d) in demands:
        weights.append((demand, int(100*d/capacity)))
        rem_capacity -= d
    weights.append((None, int(100*rem_capacity/capacity)))

    targetList = sum([[demand]*w for (demand, w) in weights], [])
    if len(targetList) > 0:
        return random.choice(targetList)
    return None

def weightedRoundRobinScheduler(node, out_edge):
    global NET
    # Get the current round robin scheduler index
    i = NET.node[node]['sch_data'][out_edge]['cur']
    seq = NET.node[node]['sch_data'][out_edge]['sch_seq']
    
    if not seq: return None
    
    stack = NET.node[node]['sch_data'][out_edge]['sch_stack']
    if __debug__:
        print "\t", node, out_edge, i, seq, stack, [(d, len(data)) for d,data in NET.node[node]['data'].items()]

    # empty the stack
    j = 0
    while j < len(stack):
        d = NET.node[node]['sch_data'][out_edge]['sch_stack'].pop()
        if len(NET.node[node]['data'][d]) > 0:
            return d
        j += 1

    # Choose the demand to schedule according to the scehuling sequence
    j = 0
    tmp_stack = []
    while j < len(seq) and len(NET.node[node]['data'][seq[i]]) == 0:
        # Append the missed element to the stack
        tmp_stack.append(seq[i])
        i = (i + 1) % len(seq)
        j += 1

    if j >= len(seq): return None

    # Append the tmp stack to the actual stack
    NET.node[node]['sch_data'][out_edge]['sch_stack'].extend(tmp_stack)
    NET.node[node]['sch_data'][out_edge]['cur'] = (i+1) % len(seq)
    return seq[i]

def roundRobinScheduler(node, out_edge):
    global NET

    # Get the current round robin scheduler index
    i = NET.node[node]['sch_data'][out_edge]['cur']
    demands_list = get_edge_attribute_from_name(NET, out_edge, 'paths')
    demands = list(set([demand[0] for demand in demands_list]))
    j = 0

    while j < len(demands) and len(NET.node[node]['data'][demands[i]]) == 0:
        i = (i + 1) % len(demands)
        j += 1
    if j == len(demands):
        return None
    else:
        NET.node[node]['sch_data'][out_edge]['cur'] = (i + 1) % len(demands)
        return demands[i]


def bufferWeightedScheduler(node, out_edge):
    demands_list = get_edge_attribute_from_name(NET, out_edge, 'paths')
    demands = [demand[0] for demand in demands_list]
    data = dict([(demand, len(NET.node[node]['data'][demand])) for demand in demands])
    sumData = sum(data.values())
    if sumData == 0: return None
    max_value = max(data.itervalues())
    max_demands = [k for k in data if data[k] == max_value]
    if len(max_demands) > 1:
        return random.choice(max_demands)
    else:
        return max_demands[0]

def maxMinFairnessMCF(C,U,Z):
    """ Max Min Fairness linear program 
         - C is the list of all the commodity indexes (equivalent to demands)
         - U is the list of all the commodity indexes (demands) with unallocated demands (initially equal to C)
         - Z is the dict of all the commodity indexes, indexed by i, that have been allocated at step i
    """

    # Create a new (empty) model and populate it below.
    model = cplex.Cplex()
    model.set_results_stream(None)
    model.set_log_stream(None)
    model.set_error_stream(None)
    model.set_warning_stream(None)

    # Add the variables in the model. We initialize with the first variable "t" 
    obj   = [1.0]              # Linear objective coefficients of the variables -- must be float
    names = ["t"]              # List of strings (variable names)
    lb    = [0.0]              # List of lower bounds (for the variables)
    ub    = [cplex.infinity]   # List of upper bounds (for the variables)
    
    # Add the commodities to the name variable and objective arrays
    # Other variables are ignored in the objective
    for demandIdx, demandData in DEMANDS.items():
        for path_name in demandData[3].keys():
            # Variable name: d<demand_index>p<path_index>
            names.append(path_name)
            obj.append(0.0)
            lb.append(0.0)
            ub.append(cplex.infinity)

    # Do not forget to effectively add the variable to the model
    model.variables.add(obj   = obj,       # Objectve factor
                        lb    = lb,        # Lower bounds
                        ub    = ub,        # Upper bounds
                        names = names);    # Variables name

    # Add the constraints in the model
    # capacity_constraint => lin_expr may be either a list of SparsePair instances or a matrix in list-of-lists format
    # senses              => senses must be either a list of single-character strings or a string containing the senses of the linear constraints. Each entry must be one of 'G', 'L', 'E', and 'R', respectively indicating greater-than, less-than, equality, and ranged constraints
    # rhs                 => rhs is a list of floats, specifying the righthand side of each linear constraint

    # Flow constraints of each demand of C (all demands) on the flow
    if ARRIVAL_TYPE != 'infinite':
        for demandIdx in C:
            weight = DEMAND_WEIGHTS[demandIdx]
            demand_constraints = [] 
            demand_senses      = []
            demand_rhs         = []
            # cplex.SparsePair does the sum: sum(value * index)
            index = [] # Variables
            value = [] # Variables' factors 

            for path_name in DEMANDS[demandIdx][3].keys():
                index.append(path_name)
                value.append(1.0)
            if index:
                demand_constraints.append(cplex.SparsePair(ind = index, val = value))
                demand_senses.append('L')
                demand_rhs.append(weight)

            model.linear_constraints.add(lin_expr = demand_constraints,
                                         senses   = demand_senses,
                                         rhs      = demand_rhs)
    # Capacity constraints (for all edges of the network)
    capacity_constraints = []
    capacity_senses      = []
    capacity_rhs         = []

    for u,v,d in NET.edges(data=True):
        index = []
        value = []
        for (demand,path_name,weight) in d['paths']:
            index.append(path_name)
            value.append(1.0)
        if index:
            capacity_constraints.append(cplex.SparsePair(ind = index, val = value))
            capacity_senses.append('L')
            capacity_rhs.append(d['traffic'])

    model.linear_constraints.add(lin_expr = list(capacity_constraints),
                                 senses   = list(capacity_senses),
                                 rhs      = list(capacity_rhs))

    # Minimum allocation constraint for all commodities that have not been allocated yet (belonging to set U)
    for demandIdx in U:
        constraint        = [] 
        senses            = []
        rhs               = []
        constraints_names = []
        # cplex.SparsePair does the sum: sum(value * index)
        index = ["t"] # Variables
        value = [-1.0] # Variables' factors 
        for path_name in DEMANDS[demandIdx][3].keys():
            index.append(path_name)
            value.append(1.0)  
        if index:
            constraint.append(cplex.SparsePair(ind = index, val = value))
            senses.append('G')
            rhs.append(0.0)
            constraints_names.append("c"+str(demandIdx))
       
        model.linear_constraints.add(lin_expr = constraint,
                                     senses   = senses,
                                     rhs      = rhs,
                                     names    = constraints_names)

    # Minimum allocation for all commodities that have been previously allocated (belonging to Z)
    # Z = [step][demandIdx]
    for step in Z.keys():
        for demandIdx in Z[step].keys():
            constraint = [] 
            senses     = []
            rhs        = []
            # cplex.SparsePair does the sum: sum(value * index)
            index = [] # Variables
            value = [] # Variables' factors 
            for path_name in DEMANDS[demandIdx][3].keys():
                index.append(path_name)
                value.append(1.0)  
            if index:
                constraint.append(cplex.SparsePair(ind = index, val = value))
                senses.append('G')
                rhs.append(Z[step][demandIdx])
           
            model.linear_constraints.add(lin_expr = constraint,
                                         senses   = senses,
                                         rhs      = rhs);

    # Our objective is to Maximize the minimum allocation
    model.objective.set_sense(model.objective.sense.maximize)
    
    return names, model


def nonBlockingTestMCF(C,U,Z,demandIndex,tk):
    """ Non-blocking test for the current state of the max-min fairness algorithm
         - C is the list of all the commodity indexes (equivalent to demands)
         - U is the list of all the commodity indexes (demands) with unallocated demands (initially equal to C)
         - Z is the dict of all the commodity indexes, indexed by i, that have been allocated at step i
    """

    # Create a new (empty) model and populate it below.
    model = cplex.Cplex()
    model.set_results_stream(None)
    model.set_log_stream(None)
    model.set_error_stream(None)
    model.set_warning_stream(None)

    # Add the variables in the model
    # Add the commodities of the examined demandIndex to the name variable and objective arrays
    obj = []    # Linear objective coefficients of the variables
    names = []  # List of strings (variable names)
    lb = []     # List of lower bounds (for the variables)
    ub = []     # List of upper bounds (for the variables)

    demandData = DEMANDS[demandIndex]
    for path_name in demandData[3].keys():
        names.append(path_name)
        obj.append(1.0)
        lb.append(0.0)
        ub.append(cplex.infinity)

    # Add the extra variables
    for demandIdx, demandData in DEMANDS.items():
        if demandIdx == demandIndex:
            continue
        for path_name in demandData[3].keys():
            # Variable name: d<demand_index>p<path_index>
            names.append(path_name)
            obj.append(0.0)
            lb.append(0.0)
            ub.append(cplex.infinity)

    # Do not forget to effectively add the variable to the model
    model.variables.add(obj   = obj,       # Objectve factor
                        lb    = lb,        # Lower bounds
                        ub    = ub,        # Upper bounds
                        names = names);    # Variables name


    # Add the constraints in the model
    # capacity_constraint => lin_expr may be either a list of SparsePair instances or a matrix in list-of-lists format
    # senses              => senses must be either a list of single-character strings or a string containing the senses of the linear constraints. Each entry must be one of 'G', 'L', 'E', and 'R', respectively indicating greater-than, less-than, equality, and ranged constraints
    # rhs                 => rhs is a list of floats, specifying the righthand side of each linear constraint

    # Flow constraints of each demand of C (all demands) on the flow
    if ARRIVAL_TYPE != 'infinite':
        for demandIdx in C:
            weight = DEMAND_WEIGHTS[demandIdx]
            demand_constraints = [] 
            demand_senses      = []
            demand_rhs         = []
            # cplex.SparsePair does the sum: sum(value * index)
            index = [] # Variables
            value = [] # Variables' factors 

            for path_name in DEMANDS[demandIdx][3].keys():
                index.append(path_name)
                value.append(1.0)
            if index:
                demand_constraints.append(cplex.SparsePair(ind = index, val = value))
                demand_senses.append('L')
                demand_rhs.append(weight)

            model.linear_constraints.add(lin_expr = demand_constraints,
                                         senses   = demand_senses,
                                         rhs      = demand_rhs)

    # Capacity constraints
    capacity_constraints = []
    capacity_senses      = []
    capacity_rhs         = []

    for u,v,d in NET.edges(data=True):
        index = []
        value = []
        for (demand,path_name,weight) in d['paths']:
            index.append(path_name)
            value.append(1.0)
        if index:
            capacity_constraints.append(cplex.SparsePair(ind = index, val = value))
            capacity_senses.append('L')
            capacity_rhs.append(d['traffic'])

    model.linear_constraints.add(lin_expr = list(capacity_constraints),
                                 senses   = list(capacity_senses),
                                 rhs      = list(capacity_rhs))


    # Minimum allocation for all commodities that have been previously allocated (belonging to Z)
    # Z = [step][demandIdx]
    for step in Z.keys():
        for demandIdx in Z[step].keys():
            constraint = [] 
            senses     = []
            rhs        = []
            # cplex.SparsePair does the sum: sum(value * index)
            index = [] # Variables
            value = [] # Variables' factors 
            for path_name in DEMANDS[demandIdx][3].keys():
                index.append(path_name)
                value.append(1.0)  
            if index:
                constraint.append(cplex.SparsePair(ind = index, val = value))
                senses.append('G')
                rhs.append(Z[step][demandIdx])
           
            model.linear_constraints.add(lin_expr = constraint,
                                         senses   = senses,
                                         rhs      = rhs)

    # Minimum allocation constraint for all commodities that have not been allocated yet (belonging to set U)
    for demandIdx in U:
        constraint        = [] 
        senses            = []
        rhs               = []
        # cplex.SparsePair does the sum: sum(value * index)
        index = [] # Variables
        value = [] # Variables' factors 
        for path_name in DEMANDS[demandIdx][3].keys():
            index.append(path_name)
            value.append(1.0)  
        if index:
            constraint.append(cplex.SparsePair(ind = index, val = value))
            senses.append('G')
            rhs.append(tk)
       
        model.linear_constraints.add(lin_expr = constraint,
                                     senses   = senses,
                                     rhs      = rhs)

    # Our objective is to Maximize the minimum allocation
    model.objective.set_sense(model.objective.sense.maximize)
    
    return names, model


def maxMinFairness():
    global NET

    # Initialization :
    C = DEMANDS.keys()      # Set of all commodities
    U = DEMANDS.keys()      # Set of all commodites whose allocation has not been fixed yet
    Z = AutoVivification()  # Dictionary of commodities whose allocation has been fixed at step i (dict indexed by the step indexes)
    F = {}                  # Resulting allocated flows 
    k = 0                   # Step counter

    while U:
        # Maximize the ith smallest allocation by solving max_min_fairness_linear_program
        names, model = maxMinFairnessMCF(C, U, Z)
        try:
            model.solve()
        except CplexSolverError, e:
            print "Exception raised during solve: " + e
            return
        
        solution = model.solution

        # Get the minimum allocation value tk for step tk -- The value is for the first variable "t" of the list names
        tk = solution.get_values(0)

        # Put the results in a dictionary
        results = AutoVivification()
        for i in range(1,len(names)):
            # We consider all other flow variables (other than variable "t") that have been allocated 
            if solution.get_values(i) > 0.0:
                match  = re.search( r'(d[0-9]+)p([0-9]+)', names[i], re.M|re.I)
                demand = match.group(1)
                path   = names[i]
                
                results[demand][path] = solution.get_values(i)

        demandsToAllocate = []

        # Allocate the demands to be allocated 
        # and determine those that can be increased
        for demandIndex in U:
            sumFlow = sum([flow for flow in results[demandIndex].values()])
            
            if __debug__:
                print k, demandIndex, tk, sumFlow, DEMANDS[demandIndex][2]
            
            # If the demand has been satisfied
            if sumFlow >= DEMANDS[demandIndex][2]:
                # The demand must be allocated
                demandsToAllocate.append(demandIndex)
                # continue to the next demand
                continue

            # If the allocated volume is greater than tk
            if not feq(sumFlow, tk) and sumFlow > tk:
                # The examined demand can be further increased
                # continue to the next demand
                continue

            # Run the non-blocking test linear progam for the current demand
            names, model = nonBlockingTestMCF(C, U, Z, demandIndex, tk)
            try:
                model.solve()
            except CplexSolverError, e:
                print "Exception raised during solve: " + e
                return

            solution = model.solution
            # Compute the allocation solution for the demand
            sumDemand_NBT = 0.0
            for i in range(len(names)):
                match  = re.search( r'(d[0-9]+)p([0-9]+)', names[i], re.M|re.I)
                demand = match.group(1)
                if demand != demandIndex:
                    continue
                if __debug__:
                    print "\t", names[i], demand, demandIndex, solution.get_values(i)
                # Increment the sum for the demand
                sumDemand_NBT += solution.get_values(i)
            if feq(sumDemand_NBT,tk):
                # The demand can be allocated, as it cannot be further increased
                demandsToAllocate.append(demandIndex)
        if __debug__:
            print k,tk, results, U, demandsToAllocate
        # When all the demands in U have been examined, modify the sets
        for demandIndex in demandsToAllocate:
            Z[k][demandIndex] = tk  # Fix the demand to tk
            U.remove(demandIndex)   # the demand has been allocated
            # Also add the commodity to the resulting dictionary, as the flow is satisfied
            F[demandIndex] = results[demandIndex]

        # Increment the step counter
        k += 1
    
    # print "\n".join("%s %s " % t for t in [(d, " ".join("%s %.2f " % tup for tup in [(p,w) for p,w in paths.items()])) for d,paths in results.items()])
    res = {}
    for demand, paths in F.items():
        for path_name, flow in paths.items():
            res[path_name] = flow
    print res

    apply_weights(NET, res)

def multiCommodityFlow():
    global NET

    model = cplex.Cplex()
    model.set_results_stream(None)
    model.set_log_stream(None)
    model.set_error_stream(None)
    model.set_warning_stream(None)

    # Add the variables in the model
    obj   = [] # Linear objective coefficients of the variables
    names = [] # List of strings (variable names)
    lb    = [] # List of lower bounds (for the variables)
    ub    = [] # List of upper bounds (fro the variables)

    for d in DEMANDS.values():
        for path_name in d[3].keys():
            obj.append(1.0)
            names.append(path_name)
            lb.append(0.0)
            ub.append(cplex.infinity)

    model.variables.add(obj = obj,          # Objectve factor
                        lb = lb,            # Lower bounds
                        ub = ub,            # Upper bounds
                        names = names)      # Variables name

    # Capacity constraints
    capacity_constraints = []
    capacity_senses      = []
    capacity_rhs         = []

    for u,v,d in NET.edges(data=True):
        index = []
        value = []
        for (demand,path_name,weight) in d['paths']:
            index.append(path_name)
            value.append(1.0)
        if index:
            capacity_constraints.append(cplex.SparsePair(ind = index, val = value))
            capacity_senses.append('L')
            capacity_rhs.append(d['traffic'])

    model.linear_constraints.add(lin_expr = list(capacity_constraints),
                                 senses   = list(capacity_senses),
                                 rhs      = list(capacity_rhs))

    if ARRIVAL_TYPE != 'infinite':
        # Demand constraints
        for demand,weight in DEMAND_WEIGHTS.items():
            demand_constraints = []
            demand_senses      = []
            demand_rhs         = []
            # cplex.SparsePair does the sum: sum(value * index)
            index = [] # Variables
            value = [] # Variables' factors 
            for path_name in DEMANDS[demand][3].keys():
                index.append(path_name)
                value.append(1.0)
            if index:
                demand_constraints.append(cplex.SparsePair(ind = index, val = value))
                demand_senses.append('L')
                demand_rhs.append(weight)

            model.linear_constraints.add(lin_expr = demand_constraints,
                                         senses   = demand_senses,
                                         rhs      = demand_rhs)

    # Our objective is to Maximize the flows.
    model.objective.set_sense(model.objective.sense.maximize)

    try:
        model.solve()
    except CplexSolverError, e:
        print "Exception raised during solve: " + e
    else:
        solution = model.solution

    # Put the results of the solution in the edges
    res = {}
    for i in range(len(names)):
        res[names[i]] = solution.get_values(i)
    print res
    
    apply_weights(NET, res)

def checkVehType(vehID):
    # Continue if the vehicle is not of type1 (the vehicles that are equipped with storage devices)
    if traci.vehicle.getTypeID(vehID) == "type1":
        return True 
    else: 
        return False

def sharesGPSInfo():
    return True # bool(random.getrandbits(1))

def dataInTransit():
    """ Return the amount of data in transit (ie. carried by vehicles) """
    return len([veh for veh in myVehicles.values() if veh.getData()])

def execScheduler(node,out_edge):
    if SCHEDULER_TYPE == 'RR':
        return roundRobinScheduler(node,out_edge)
    elif SCHEDULER_TYPE == 'BUF':
        return bufferWeightedScheduler(node, out_edge)
    elif SCHEDULER_TYPE in ('MCF', 'MMF'):
        return weightedScheduler_full(node, out_edge)
    elif SCHEDULER_TYPE in ('MCF-RR', 'MMF-RR'):
        return weightedRoundRobinScheduler(node, out_edge)

def in_offloading_spot(node, vehID, step):
    """ Vehicle vehID just arrived at offloading spot node at step """
    global myVehicles, NET

    # If the arrival type is infinite, regenerate some data
    if ARRIVAL_TYPE == 'infinite' and node in SOURCES:
        regenerate_data_at_source(node)

    veh = myVehicles[vehID]
    if veh.isSharingGPSInfo():
        # We have access to the vehicle's GPS information
        # Get the vehicle destination (next-hop logical node)
        vehDest = veh.getDestination()
        # Pass the sequence to the scheduler to get the data to load
        demand = execScheduler(node, vehDest)

        if not demand: # Do nothing
            return

        # Load the data on the vehicle
        if len(NET.node[node]['data'][demand]) > 0:
            data = NET.node[node]['data'][demand].pop()
            if node in SOURCES:
                data.loadTime = step
            else:
                data.endStop(node, step)

            veh.loadData(data)

            if __debug__:
                print "[%d] Vehicle %s arrived at node %s takes data %s" % (step, vehID, node, data)

    else:
        pass


def out_offloading_spot(node, vehID, step):
    """ Vehicle vehID just left at offloading spot node at step """
    global myVehicles, NET

    veh = myVehicles[vehID]

    # data drop-off
    data = veh.unloadData()
    if data:
        if node in TARGETS:
            # Check whether the data has arrived at the right destination
            if data.getDemand() not in NET.node[node]['data'].keys():
                print "Forwarding error"
                return

            data.endTime = step
        else:
            data.startStop(node, step)
        NET.node[node]['data'][data.getDemand()].append(data)
        if __debug__:
            print "[%d] Vehicle %s left at node %s brought data %s" % (step, vehID, node, data)


def run():
    """ execute the TraCI control loop
        -  """

    global myVehicles, NET
    
    traci.init(PORT)

    if ARRIVAL_TYPE == 'Poisson':
        arrivals = generateArrivals()

    step = 0
    print_count = 0
    output_file = 'results/I1_storage_%s_%s.txt' % (SCHEDULER_TYPE, ARRIVAL_TYPE)
    f = open(output_file, 'w')
    f.write("step;%s\n" % (';'.join([d for d in NET.node['I1']['data'].keys()])))
    while step < SIM_TIME:
        if print_count == 1000:
            print "[%s]" % step
            print_count = 0

        # traci.simulation.getMinExpectedNumber()
        traci.simulationStep()

        if ARRIVAL_TYPE == 'Poisson' :
            # Generate the data in the corresponding buffer according to a Poisson process
            while (len(arrivals) > 0) and (math.floor(arrivals[0][1]) == step):
                d = arrivals.pop(0)
                src = DEMANDS[d[0]][0]
                NET.node[src]['data'][d[0]].append(Data(d[0], startTime = step))

        # Get the vehicles ids that entered the simulation ("departed")
        enteredSim = traci.simulation.getDepartedIDList()
        for vehID in enteredSim:
            if not checkVehType(vehID):
                continue

            # Add the vehicle to the myVehicles dictionary
            vehRoute = traci.vehicle.getRoute(vehID)
            if not vehID in myVehicles:
                myVehicles[vehID] = Vehicle(vehID, vehRoute)

            edgeID = traci.vehicle.getRoadID(vehID)
            # Get the offloading spot from the edge
            offloading_spot = get_node_from_edge(NET, edgeID, 'in')
            # Load the data at the offloading spot
            in_offloading_spot(offloading_spot, vehID, step)


        # Get the vehicles ids that exited the simulation ("arrived")
        exitedSim = traci.simulation.getArrivedIDList()
        for vehID in exitedSim:
            if vehID not in myVehicles:
                continue
            
            veh = myVehicles[vehID]
            if not veh.hasData(): 
                continue

            edgeID = veh.getDestination()
            # Get the offloading spot from the edge
            offloading_spot = get_node_from_edge(NET, edgeID, 'out')
             # Unload the data at the offloading spot
            out_offloading_spot(offloading_spot, vehID, step)
            # Delete the vehicle from the myVehicles dictionary
            del myVehicles[vehID]

        # Print STATS
        if __debug__:
            nodes = ['S1', 'S2', 'I1', 'I2', 'I3', 'D1', 'D2']
            for demand in DEMANDS.keys():
                print "%s: %s" % (demand, " ".join("%s %s " % tup for tup in [(node, len(NET.node[node]['data'][demand])) for node in nodes if demand in NET.node[node]['data']]))
            # for node, demand in [(node, [(demand, len(data)) for demand,data in d['data'].items()]) for node, d in NET.nodes(data=True)]:
            #     print "%s: %s" % (node, " ".join("(%s,%s)" % tup for tup in demand))

        f.write("%s;%s\n" % (step,';'.join([str(len(NET.node['I1']['data'][d])) for d in NET.node['I1']['data'].keys()])))

        step += 1
        print_count += 1

        if ARRIVAL_TYPE == 'fixed':
            if sum([DEMANDS[demand][2] == len(NET.node[d[1]]['data'][demand]) for (demand, d) in DEMANDS.items()]) == len(DEMANDS):
                break

    # Compute statistics on the data arrived at destination
    for target in TARGETS:
        for demand in NET.node[target]['data'].keys():
            sumTimes = 0
            dataLength = len(NET.node[target]['data'][demand])
            for data in NET.node[target]['data'][demand]:
                if data.loadTime < WARMUP_TIME:
                    continue
                sumTimes += (data.endTime - data.loadTime)
            if dataLength > 0:
                print "%s;%s;%s;%g;%g;%g" % (SCHEDULER_TYPE, ARRIVAL_TYPE, demand, dataLength, sumTimes/dataLength, dataLength / (step))
            else:
                print "%s;%s;%s;%g;%g;%g" % (SCHEDULER_TYPE, ARRIVAL_TYPE, demand, dataLength, 0, dataLength / (step))

    traci.close()
    f.close()
    sys.stdout.flush()

def get_options():
    optParser = optparse.OptionParser()
    optParser.add_option("--nogui", action="store_true", default=False, help="run the commandline version of sumo")
    optParser.add_option("-s", action="store", dest='scheduler', help="define the scheduler")
    optParser.add_option("-a", action="store", dest='arrival', help='define the data arrival type')
    options, args = optParser.parse_args()
    return options

def print_net_stats():
    for n in NET.nodes():
        print '## %s: [ %s]' % (n,' '.join("%s %s " % tup for tup in [(d,len(data)) for d,data in NET.node[n]['data'].items()]))
        if 'sch_data' in NET.node[n]:
            for e in NET.node[n]['sch_data'].keys():
                print '\t', e, NET.node[n]['sch_data'][e]
    print
    for u,v in NET.edges():
        print '## %s (traffic:%s, weight:%s)' % (NET[u][v]['name'], NET[u][v]['traffic'], NET[u][v]['weight'])
        for p in NET[u][v]['paths']:
            print '\t', p

# this is the main entry point of this script
if __name__ == "__main__":
    sumoBinary = checkBinary('sumo')

    options = get_options()


    if options.scheduler in SCHEDULERS:
        SCHEDULER_TYPE = options.scheduler
    if options.arrival in ARRIVALS:
        ARRIVAL_TYPE = options.arrival

    print "%s / %s" % (SCHEDULER_TYPE, ARRIVAL_TYPE)
    generate_routefile()
    init()

    if SCHEDULER_TYPE in ['MCF', 'MCF-RR']:
        multiCommodityFlow()
    if SCHEDULER_TYPE in ['MMF', 'MMF-RR']:
        maxMinFairness()

    if __debug__: print_net_stats()

    # this is the normal way of using traci. sumo is started as a
    # subprocess and then the python script connects and runs
    sumoProcess = subprocess.Popen([sumoBinary, "-c", "sim.sumo.cfg", "--remote-port", str(PORT)], stdout=sys.stdout, stderr=sys.stderr)
    run()
    sumoProcess.wait()
