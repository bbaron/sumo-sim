library(tools)
library(reshape2)
library(ggplot2)

options(echo=TRUE) # if you want see commands in output file
args <- commandArgs(trailingOnly = TRUE)
print(args)
print(args[1])

input_file = args[1]
output_file = paste(file_path_sans_ext(args[1]), "pdf", sep=".")

pdf(output_file, width = 10, height = 3.5)

s <- read.csv(input_file, sep=";", quote="\"")
ggplot(s, aes(step)) + 
	geom_line(aes(y = d1, colour = "d1")) + 
	geom_line(aes(y = d2, colour = "d2")) + 
	geom_line(aes(y = d3, colour = "d3")) + 
	ylab("Amount of data in buffer") + 
	xlab("Simulation timestep (second)")