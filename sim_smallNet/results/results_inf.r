library(tools)
library(reshape2)
library(ggplot2)

options(echo=TRUE) # if you want see commands in output file
args <- commandArgs(trailingOnly = TRUE)
print(args)
print(args[1])

input_file = args[1]
output_file = paste(file_path_sans_ext(args[1]), "pdf", sep=".")

pdf(output_file, width = 10, height = 3.5)

results <- read.table(input_file, sep=";", quote="\"")
results$grp <- paste(results$V1, results$V2)
ggplot(results, aes(x=V4, y=V7, fill=V7)) + geom_bar(stat = 'identity', position = 'stack')+facet_grid(~ grp)+xlab("Configuration") + ylab("Throughput (data chunk/second)")
