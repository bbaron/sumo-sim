import csv
import os

class AutoVivification(dict):
    """Implementation of perl's autovivification feature."""
    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value

filename = "/Users/ben/Desktop/results-sim-max-throughput_opt.txt"
fname, fext = os.path.splitext(filename)
f_out = open(fname+'_opt'+fext, 'w')

res = AutoVivification()
with open(filename, 'r') as f_in:
	reader = csv.reader(f_in, delimiter=";")
	for row in reader:
		d,sch,arr,l,ret,nbr_data,delay,throughput,opt = row
		res[(sch,arr,l,ret)][d] = (int(nbr_data),float(delay),float(throughput),float(opt))

for ((sch,arr,l,ret), data) in res.items():
	aggregated_throughput = 0.0
	aggregated_opt = 0.0
	for d in data.keys():
		(nbr_data,delay,throughput,opt) = data[d]
		aggregated_throughput += throughput
		aggregated_opt += opt
		f_out.write("%s;%s;%s;%s;%s;%s;%s;%s;%s\n" % (d,sch,arr,l,ret,nbr_data,delay,throughput,opt))
	f_out.write("%s;%s;%s;%s;%s;%s;%s;%s;%s\n" % ("all",sch,arr,l,ret,0,0,aggregated_throughput,aggregated_opt))

f_out.close()