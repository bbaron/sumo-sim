from pyproj import Proj
import networkx as nx
from tools import Data, Demand, Vehicle

proj = Proj("+proj=lcc +lat_1=44 +lat_2=49 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,-0,-0,-0,0 +units=m +no_defs") # EPSG 2154, Lambert93

NB_PROCESSES = 3
MAX_DIST = 300000	# Maximal distance in meters between two offloading spots
STORAGE_SIZE = 1e9  # Storage size of the vehicles (here: 1 TB per vehicle)
LEAKAGE = 0.3  # Leakage of logical links
MAX_LOGICAL_PATHS = 50
RETRANSMISSION = 'local'
ALPHA = 0.75 # EWMA parameter for the traveltime on the logical links
# alpha * x_t + (1-alpha) * x_t-1

# the port used for communicating with your sumo instance
PORT = 8874

# Parameters of the simulation
PENETRATION_RATIO = 0.1
SIM_TIME = 300000 # Simulation time (in seconds)
STOP_PROB = 0
SCHEDULER_TYPE = 'MCF' # enum('RR', 'BUF', 'MCF')
ARRIVAL_TYPE = 'Poisson' # arrivalType in enum('fixed', 'Poisson', 'infinite')
SCHEDULERS = ['RR', 'BUF', 'MCF', 'MMF', 'MCF-RR', 'MMF-RR']
ARRIVALS = ['fixed', 'Poisson', 'infinite']
WARMUP_TIME = 0
RATE = 0.25

ROAD_NETWORK = nx.DiGraph()
NET = nx.DiGraph()

VEHICLES = dict() # Global dictionary that stores all of the vehicles equipped with data storage devices

# proj(*reversed(Geocoder.geocode('Marseille, France')[0].coordinates))
LOCATIONS = {
	'S1': {'coords': (652470.6423107067, 6862036.802915546)},  # Paris, Lambert93
	'D1': {'coords': (842663.3623294821, 6519929.066623623)},  # Lyon, Lambert93
	'D2': {'coords': (417243.06268375093, 6421812.053793158)}, # Bordeaux, Lambert93
	'D3': {'coords': (892388.6584137226, 6247033.208004002)}   # Marseille, Lambert93
}

DEMANDS = dict()
DEMAND_WEIGHTS = dict()
SOURCES = set()
TARGETS = set()
