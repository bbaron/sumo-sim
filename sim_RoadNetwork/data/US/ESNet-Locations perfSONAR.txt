anl-pt1.es.net; 		10Gbit/s; 			Lemont, IL		;Argonne National Laboratory							;41.7129; -87.9605
bnl-pt1.es.net; 		10Gbit/s; 			Upton, NY		;Brookhaven National Laboratory							;40.8703; -72.8805
bois-pt1.es.net;		10Gbit/s; 			Boise, ID		;BOIS 													;43.5659; -116.1881	
chic-pt1.es.net;		10Gbit/s; 			Chicago, IL		;CHIC 													;41.8964; -87.6431
fnal-pt1.es.net;		10Gbit/s; 			Batavia, IL 	;Fermi National Laboratory								;41.8447; -88.3081
ga-pt1.es.net; 		10Gbit/s; 			San Diego, CA 		;General Atomics										;32.8942; -117.2381
hous-pt1.es.net;		10Gbit/s; 			Houston, TX 	;HOUS 													;29.9419; -95.4195
jgi-pt1.es.net; 		10Gbit/s; 			Walnut Creek, CA ;Joint Genome Institute 								;37.9306; -122.0228
lbl-pt1.es.net; 		10Gbit/s; 			Berkeley, CA 	;Lawrence Berkeley National Laboratory					;37.8770; -122.2527
llnl-pt1.es.net;		10Gbit/s; 			Livermore, CA 	;Lawrence Livermore National Lab 						;37.6569; -121.7267
nash-pt1.es.net;		10Gbit/s; 			Nashville, TN 	;NASH 													;36.1030; -86.7560
nersc-pt1.es.net;	10Gbit/s; 			Oakland, CA 		;National Energy Research Scientific Computing Center 	;37.8086; -122.2675
ornl-pt1.es.net;		10Gbit/s; 			Oak Ridge, TN 	;Oak Ridge National Laboratory							;36.0414; -84.2131
pppl-pt1.es.net;		10Gbit/s; 			Princeton, NJ	;Princeton Plasma Physics Lab 							;40.3453; -74.6158
sdsc-pt1.es.net;		10Gbit/s; 			La Jolla, CA 	;San Diego Supercomputing Center 						;32.8836; -117.2386
slac-pt1.es.net;		10Gbit/s; 			Palo Alto, CA 	;Stanford Linear Accelerator 							;37.4211; -122.2044
snll-pt1.es.net;		10Gbit/s; 			Livermore, CA 	;Sandia National Laboratory 							;37.6794; -121.6983
sunn-pt1.es.net;		10Gbit/s; 			Sunnyvale, CA 	;SUNN 													;37.3739; -121.9911
wash-pt1.es.net;		10Gbit/s; 			Washington, DC 	;WASH 													;38.9203; -77.2123