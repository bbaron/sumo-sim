from __future__ import division
import os, sys
import csv
import networkx as nx
from xlrd import open_workbook, XL_CELL_TEXT

sys.path.append(os.path.join(os.path.dirname('__file__'), '..'))
from tools import *
from settings import *

def generate_graph(filename):
    """ Generate the graph instance a trip matrix
    """

    nodes = {} # Dictionary of all the nodes in the road network indexed by the junction coordinates (x,y)
    G = nx.DiGraph() # Create a directed graph that represents the road network
    edge_id = 1
    node_id = 1
    with open(filename, 'rb') as f:
        reader = csv.reader(f)
        for row in reader:
            node1 = (float(row[1]), float(row[2]))
            node2 = (float(row[3]), float(row[4]))
            
            flow = float(row[8])
            dist = float(row[5])
            travelTime = float(row[6])

            if node1 not in nodes:
                nodes[node1] = 'n'+str(node_id)
                node_id += 1
            node1_name = nodes[node1]
            if node2 not in nodes:
                nodes[node2] = 'n'+str(node_id)
                node_id += 1
            node2_name = nodes[node2]
            edge_name = 'e'+str(edge_id)
            edge_id += 1

            # Add the edge + attributes in the graph G
            G.add_edge(node1, node2, name=edge_name+'-a', aadt=flow, color="red", distance=dist, flows=[], travelTime=travelTime, speed=dist/travelTime)
            G.add_edge(node2, node1, name=edge_name+'-b', aadt=flow, color="red", distance=dist, flows=[], travelTime=travelTime, speed=dist/travelTime)
    
            G.node[node1]['name'] = node1_name
            G.node[node2]['name'] = node2_name
    # Return the graph
    return G

def generate_network(G, filename='sim'):
    """ Generate the SUMO node and edge files """

    nodes_filename = filename+'.nod.xml'
    edges_filename = filename+'.edg.xml'
    connections_filename = filename+'.con.xml'

    with open(nodes_filename, 'w') as nodes:
        print >> nodes, '<nodes>'
        for (u,v),d in G.nodes(data=True):
            print >> nodes, '    <node id="%s" x="%s" y="%s" />' % (d['name'],u,v)
        print >> nodes, '</nodes>'

    with open(edges_filename, 'w') as edges:
        print >> edges, '<edges>'
        for (u,v,d) in G.edges(data=True):
            from_node = G.node[u]['name'] 
            to_node   = G.node[v]['name']
            edge_name = d['name']
            print >> edges, '    <edge from="%s" id="%s" to="%s" numLanes="7" speed="%g" />' % (from_node, edge_name, to_node, d['speed'])
        print >> edges, '</edges>'

def generate_routefile(G, out_filename="sim.rou.xml"):
    """ This function generates the route file from the attributes given in the command line """

    with open(out_filename, "w") as routes:
        print >> routes, """<routes>
    <!-- Types of vehicles -->
    <vTypeDistribution id="typedist1">"""
        print >> routes, '        <vType id="type1" vClass="passenger" probability="%.2f" color="0,0,1" />' % (1.0-PENETRATION_RATIO)
        print >> routes, '        <vType id="type2" vClass="passenger" probability="%.2f" color="0,1,1" />' % (PENETRATION_RATIO)
        print >> routes, "    </vTypeDistribution>"

        print >> routes, '    <!-- Flows -->'
        for (i,j) in G.edges():
            i_name, j_name = G.node[i]['name'], G.node[j]['name']
            
            # Define a flow with the previous route distribution
            flow_name = 'flow-%s-%s' % (i_name, j_name)
            route_name = 'route-%s-%s' % (i_name, j_name)
            traffic = G[i][j]['aadt']
            if traffic > 0:
                print >> routes, '    <flow id="%s" departPos="0" departLane="random" departSpeed="max" type="typedist1" begin="0.00" vehsPerHour="%g">' % (flow_name, traffic/24)
                print >> routes, '        <route id="%s" edges="%s" />' % (route_name, G[i][j]['name'])
                print >> routes, '    </flow>'
        print >> routes, '</routes>'


if __name__ == "__main__":
    print "Generate the France and overlay graph"
    road_network = generate_graph("data/FR/fr-overlay.txt")
    generate_network(road_network)
    generate_routefile(road_network)
