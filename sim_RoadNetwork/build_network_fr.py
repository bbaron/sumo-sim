from __future__ import division
import os, sys
import csv
import networkx as nx
from xlrd import open_workbook, XL_CELL_TEXT

sys.path.append(os.path.join(os.path.dirname('__file__'), '..'))
from tools import *

def generate_graph(file):
    """ Generate the graph instance from the Excel file file
    """
    reload(sys)
    sys.setdefaultencoding("utf-8")
    
    nodes = {} # Dictionary of all the nodes in the road network indexed by the junction coordinates (x,y)
    G = nx.DiGraph() # Create a directed graph that represents the road network
    
    # Build the graph with the coordinates (xD,yD) or (xF,yF) as the nodes using the edges (road stretches)
    book = open_workbook(file)
    sheet = book.sheet_by_index(0)

    # Generate the graph from each line of the file
    # Adjust the AADT on each link, the newer one takes over the older ones
    km = 0
    i = 0
    node_id = 1
    for rownum in range(1, sheet.nrows):
        row = sheet.row_values(rownum)
        try:
            node1 = float(row[9]), float(row[10])   # The one extermity is defined by the coordinates (xD,yD)
            node2 = float(row[17]), float(row[18])  # The other extermity is defined by the coordinates (xF,yF)
            if row[31] != "": AADT = int(row[31])   # AADT from year N
            elif row[34] != "": AADT = int(row[34]) # AADT from year N-1
            elif row[37] != "": AADT = int(row[37]) # AADT from year N-2
            elif row[40] != "": AADT = int(row[40]) # AADT from year N-3
            elif row[43] != "": AADT = int(row[43]) # AADT from year N-4
            elif row[46] != "": AADT = int(row[46]) # AADT from year N-5
            else: AADT = 0
            i += 1
            if row[3] != "": km = km + row[3]
        except ValueError,e:
            print "error",e,"on line",rownum
        speed = get_speed(row[75], row[83]) # Get the speed limit from the road segment characteristics
        travelTime = row[3]/speed           # Get the travel time from the distance of road segment
        if node1 not in nodes:
            nodes[node1] = 'n'+str(node_id)
            node_id += 1
        node1_name = nodes[node1]
        if node2 not in nodes:
            nodes[node2] = 'n'+str(node_id)
            node_id += 1
        node2_name = nodes[node2]
        edge_name = 'e'+str(i)

        # Add the edge + attributes in the graph G
        G.add_edge(node1, node2, name=edge_name+'-a', row=row[0], aadt=AADT/2, color="red", distance=row[3], flows=[], travelTime=travelTime, profile = row[75], index = row[83], speed=speed)
        G.add_edge(node2, node1, name=edge_name+'-b', row=row[0], aadt=AADT/2, color="red", distance=row[3], flows=[], travelTime=travelTime, profile = row[75], index = row[83], speed=speed)

        G.node[node1]['name'] = node1_name
        G.node[node2]['name'] = node2_name
        # Distance is in meters
    
    # Return the graph
    return G

def get_speed(profile, index):
    """ Return the speed in m/s according to road characteristics in the SETRA files
    """
    if profile in ['1V', '2V<7', '=2*1V']: return 25.0  # 90 km/h
    if index in ['1', '5']: return 36.11                # 130 km/h
    if index in ['2', '3']: return 30.56                # 110 km/h
    if index in ['4']: return 25.0                      # 90 km/h
    else: return 25.0                                   # 90 km/h (default)

def analyze_chosen_stations(G, f):
    """ Analyses file f containing data on the chosen charging stations
    """
    with open(f, 'rb') as csvfile:
        H = nx.Graph()
        rows = csv.reader(csvfile, delimiter=',')
        firstRow = rows.next() # First line: column titles
        node_id = 1
        for row in rows:
            node = float(row[16]), float(row[17])
            junction = get_closest_junction(G, node)
            node_name = 'os'+str(node_id)
            H.add_node(junction, name=node_name, originalPoint=node, weight=float(row[18]))
            node_id += 1
        return H

def generate_network(G, filename='sim'):
    """ Generate the SUMO node and edge files """

    nodes_filename = filename+'.nod.xml'
    edges_filename = filename+'.edg.xml'
    connections_filename = filename+'.con.xml'

    with open(nodes_filename, 'w') as nodes:
        print >> nodes, '<nodes>'
        for (u,v),d in G.nodes(data=True):
            print >> nodes, '    <node id="%s" x="%s" y="%s" />' % (d['name'],u,v)
        print >> nodes, '</nodes>'

    with open(edges_filename, 'w') as edges:
        print >> edges, '<edges>'
        for (u,v,d) in G.edges(data=True):
            from_node = G.node[u]['name'] 
            to_node   = G.node[v]['name']
            edge_name = d['name']
            print >> edges, '    <edge from="%s" id="%s" to="%s" numLanes="7" speed="%g" />' % (from_node, edge_name, to_node, d['speed'])
            # print >> edges, '    <edge from="%s" id="%s" to="%s" numLanes="7" speed="%g" />' % (to_node, edge_name, from_node, d['speed'])
        print >> edges, '</edges>'

if __name__ == "__main__":
    print "Generate the France and overlay graph"
    road_network = generate_graph("data/FR/traffic-fr-2011.xls")
    generate_network(road_network)
    points = analyze_chosen_stations(road_network, 'data/FR/stations-fr-150km.csv')
